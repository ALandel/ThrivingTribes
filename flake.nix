{
  inputs.nixpkgs.url =  "github:nixos/nixpkgs/nixos-24.05";
  outputs =
    { self, nixpkgs }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in {
        devShell .x86_64-linux = pkgs.mkShell rec {
          nativeBuildInputs = with pkgs; [
            pkg-config
          ];
          buildInputs = with pkgs; [
            clang
            llvmPackages.bintools

            rustc
            rustfmt
            cargo
            rustPlatform.rustcSrc
            rustPlatform.rustLibSrc

 	    alsa-lib
            pkg-config
            sqlite.dev
            udev
            vulkan-loader

            # vulkan
            vulkan-tools
            vulkan-headers
            vulkan-loader
            vulkan-validation-layers            

            # To use the x11 feature
            xorg.libX11
            xorg.libXcursor
            xorg.libXi
            xorg.libXrandr

            # To use the wayland feature
            libxkbcommon
            wayland
          ];
          LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath buildInputs;
        };
    };
}
