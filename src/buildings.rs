use crate::combat::Item;
use crate::equipment::Inventory;
use bevy::log;
use bevy::prelude::*;
use bevy::sprite::MaterialMesh2dBundle;

use crate::game_map::grid_index_to_coord;
use crate::game_map::{Buildings, MapData, TileSetData};
use crate::hero::PlayerCharacter;
use crate::input::{Input, InputEvent};
use crate::setup_game;

pub struct BuildingsPlugin;

impl Plugin for BuildingsPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<UseBuildingEvent>()
            .add_systems(Startup, spawn_buildings.after(setup_game))
            .add_systems(
                FixedUpdate,
                (
                    check_use_building,
                    update_buildings,
                    handle_use_building_event,
                ),
            );
    }
}

#[derive(Component, Debug)]
pub struct FoodConverter;

#[derive(Component, Debug)]
pub struct ManaTree;

pub fn spawn_buildings(
    map_data: Res<MapData>,
    mut commands: Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    info!("inside spawn_buildings");
    let tile_set = map_data
        .tile_sets
        .get("Slates_v_2_32x32px_orthogonal_tileset_by_Ivan_Voirol_1")
        .unwrap();

    for building in &map_data.buildings {
        info!("{building:?}");
        match building {
            Buildings::FoodConverter { position } => {
                let tile_x = 14;
                let tile_y = 19;

                let bundle = create_mesh_bundle(&mut materials, tile_set, position, tile_x, tile_y);

                commands.spawn((FoodConverter, bundle));
            }
            Buildings::ManaTree { position } => {
                let tiles_x = 12..=13;
                let tiles_y = 19..=21;

                for (y, tile_y) in tiles_y.rev().enumerate() {
                    for (x, tile_x) in tiles_x.clone().enumerate() {
                        let offset = grid_index_to_coord(x as i32, y as i32);
                        let position = [position[0] + offset.x, position[1] - offset.y];
                        log::info!("{x}:{y} -> {}:{}", position[0], position[1]);
                        let bundle =
                            create_mesh_bundle(&mut materials, tile_set, &position, tile_x, tile_y);

                        commands.spawn((ManaTree, bundle));
                    }
                }
            }
        }
    }
}

fn create_mesh_bundle(
    materials: &mut ResMut<Assets<ColorMaterial>>,
    tile_set: &TileSetData,
    position: &[f32; 2],
    tile_x: usize,
    tile_y: usize,
) -> (Mesh2d, MeshMaterial2d<ColorMaterial>, Transform) {
    let mut transform =
        Transform::from_xyz(0., 0., 2.).with_scale(Vec3::splat(tile_set.count_width as f32));
    let texture_handle = &tile_set.texture;
    let meshes_raw = &tile_set.mesh;

    transform.translation.x = position[0];
    transform.translation.y = -position[1];

    let bundle = (
        Mesh2d(meshes_raw[tile_x][tile_y].clone().into()),
        MeshMaterial2d(materials.add(ColorMaterial::from(texture_handle.clone()))),
        transform,
    );
    bundle
}

pub fn update_buildings() {}

#[derive(Event)]
pub struct UseBuildingEvent {
    player: Entity,
    building: Entity,
}

pub fn check_use_building(
    mut input_event: EventReader<InputEvent>,
    mut output_event: EventWriter<UseBuildingEvent>,
    buildings: Query<(Entity, &Transform), With<FoodConverter>>,
    players: Query<(Entity, &Transform), With<PlayerCharacter>>,
) {
    let mut has_events = false;
    for event in input_event.read() {
        if event.inputs.contains(&Input::Use) {
            has_events = true;
        }
    }

    if has_events {
        for (building, building_transform) in &buildings {
            for (player, player_transform) in &players {
                let building_pos = building_transform.translation.truncate();
                let player_pos = player_transform.translation.truncate();

                if building_pos.distance(player_pos) <= 100. {
                    output_event.send(UseBuildingEvent { player, building });
                }
            }
        }
    }
}

pub fn handle_use_building_event(
    mut events: EventReader<UseBuildingEvent>,
    mut players: Query<&mut Inventory, With<PlayerCharacter>>,
) {
    for event in events.read() {
        let mut inventory = players.get_mut(event.player).unwrap();

        if inventory.has(&Item::ManaDust, 8) {
            inventory.remove(&Item::ManaDust, 8);
            inventory.add(Item::ManaPotion, 1);
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use bevy::sprite::ColorMaterialPlugin;
    use std::collections::HashMap;
    #[test]
    fn test_spawn_buildings() {
        let mut tile_sets = HashMap::new();
        tile_sets.insert(
            "Slates_v_2_32x32px_orthogonal_tileset_by_Ivan_Voirol_1".to_string(),
            TileSetData {
                texture: Default::default(),
                mesh: vec![],
                image_path: "".to_string(),
                count_height: 0,
                count_width: 0,
            },
        );

        let mut app = App::new();
        app.add_plugins(TaskPoolPlugin::default())
            .add_plugins(AssetPlugin::default())
            // app.insert_resource(ColorMaterial::default());
            .init_asset::<Shader>()
            .init_asset::<Mesh>()
            .init_asset::<Image>()
            .add_plugins(ColorMaterialPlugin::default())
            .add_systems(Startup, load_map_data)
            //                   v this v
            .add_systems(Update, spawn_buildings)
            //                   ^ this ^
            .insert_resource(MapData {
                tile_sets,
                hero_spawn: Default::default(),
                buildings: vec![Buildings::ManaTree {
                    position: [12.3, 45.6],
                }],
            })
            .update();

        let world = app.world_mut();

        let mut mana_tree_query = world.query::<&ManaTree>();
        let _ = mana_tree_query.single(&world);

        let mut meshes_query = world.query::<&Mesh2d>();
        let meshes: Vec<_> = meshes_query.iter(&world).collect();

        assert_eq!(meshes.len(), 4);
    }

    fn load_map_data(
        mut assets_meshes: ResMut<Assets<Mesh>>,
        asset_server: Res<AssetServer>,
        mut map_data: ResMut<MapData>,
    ) {
        use crate::game_map::load_tileset;
        use crate::ldtk_import::import;

        let map = import("assets/maps/base.ldtk").unwrap();

        let mut tileset_uid_to_name = HashMap::new();

        for tileset in &map.defs.tilesets {
            let path = &tileset.rel_path;
            let path = if path.starts_with("../textures/") {
                path.replace("../textures/", "textures/")
            } else {
                path.clone()
            };
            let w = tileset.__c_wid as usize;
            let h = tileset.__c_hei as usize;
            let id = tileset.identifier.clone();

            tileset_uid_to_name.insert(tileset.uid, id.clone());

            let tile_set = load_tileset(&mut assets_meshes, &asset_server, path, w, h);

            map_data.tile_sets.insert(id, tile_set);
        }
    }
}
