use std::collections::BTreeMap;

use crate::debug::DebugInfo;
use crate::game_map::{add_tile, coord_to_grid_index, DrawnMapTile, MapData};
use crate::hero::PlayerCharacter;
use crate::input::InputEvent;
use crate::model::UI;
use bevy::asset::{AssetServer, Assets};
use bevy::audio::PlaybackMode;
use bevy::diagnostic::{DiagnosticsStore, FrameTimeDiagnosticsPlugin};
use bevy::input::mouse::MouseButtonInput;
use bevy::math::Vec2;
use bevy::prelude::*;
use bevy::ui::RelativeCursorPosition;
use bevy::window::PrimaryWindow;

#[derive(Event)]
pub struct CreateTileEvent {
    tile_x: usize,
    tile_y: usize,
    grid_x: i32,
    grid_y: i32,
    tile_set: String,
}

pub fn create_tile_ev_handler(
    mut commands: Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    drawn_map_tiles: Query<(Entity, &DrawnMapTile)>,
    mut ev_tile_button: EventReader<CreateTileEvent>,
    map_data: Res<MapData>,
) {
    for event in ev_tile_button.read() {
        let &CreateTileEvent {
            grid_x,
            grid_y,
            tile_x,
            tile_y,
            tile_set: _,
        } = event;
        let tile_set_name = event.tile_set.clone();

        for (entity, tile) in &drawn_map_tiles {
            if tile.pos_x == grid_x && tile.pos_y == grid_y {
                commands.entity(entity).despawn();
                println!("despawned at {grid_x}:{grid_y}");
                break;
            }
        }

        let tile_set = map_data.tile_sets.get(&tile_set_name).unwrap();
        let tiles_texture_handle = &tile_set.texture;
        let tiles_meshes_raw = &tile_set.mesh;

        add_tile(
            &mut commands,
            &tiles_meshes_raw,
            &mut materials,
            &tiles_texture_handle,
            tile_set_name,
            tile_x,
            tile_y,
            grid_x,
            grid_y,
            2,
        );
    }
}

pub fn mouse_button_events(
    mut mousebtn_evr: EventReader<MouseButtonInput>,
    q_windows: Query<&Window, With<PrimaryWindow>>,
    relative_cursor_position_query: Query<&RelativeCursorPosition>,
    q_camera: Query<&GlobalTransform, With<Camera>>,
    selected_tile: Res<SelectedTile>,
    mut ev_tile_button: EventWriter<CreateTileEvent>,
) {
    if !selected_tile.selected {
        return;
    }

    // Games typically only have one window (the primary window)
    let window = q_windows.single();
    let screen_position = if let Some(position) = window.cursor_position() {
        position
    } else {
        return;
    };
    let window_width = window.width() as i32;
    let window_height = window.height() as i32;

    for event in mousebtn_evr.read() {
        let (x, y) = {
            if !event.state.is_pressed() {
                continue;
            }

            for relative_cursor_position in &relative_cursor_position_query {
                if relative_cursor_position.mouse_over() {
                    return;
                }
            }

            let camera = q_camera.single().translation().truncate();

            let x_pos = camera.x + screen_position.x;
            let y_pos = camera.y - screen_position.y;

            // screen-size offset
            let mut x_pos = x_pos - window_width as f32 / 2.;
            let mut y_pos = y_pos + window_height as f32 / 2.;

            // adjust to grid

            x_pos = (x_pos / 50.).round() * 50.;
            y_pos = (y_pos / 50.).round() * 50.;

            coord_to_grid_index(Vec2::new(x_pos, y_pos), 2)
        };

        let x_text = selected_tile.x;
        let y_text = selected_tile.y;

        ev_tile_button.send(CreateTileEvent {
            grid_x: x,
            grid_y: y,
            tile_x: x_text,
            tile_y: y_text,
            tile_set: selected_tile.tile_set.clone(),
        });
    }
}

#[derive(Component, Debug)]
pub struct TileButton {
    x: usize,
    y: usize,
    tile_set: String,
}

#[derive(Resource, Debug)]
pub struct SelectedTile {
    pub selected: bool,
    pub x: usize,
    pub y: usize,
    pub tile_set: String,
}

pub fn tile_button_handler(
    mut interaction_query: Query<(&Interaction, &TileButton), (Changed<Interaction>, With<Button>)>,
    mut selected_tile: ResMut<SelectedTile>,
) {
    for (interaction, tile_button) in &mut interaction_query {
        match interaction {
            Interaction::Pressed => {
                selected_tile.selected =
                    if selected_tile.x == tile_button.x && selected_tile.y == tile_button.y {
                        !selected_tile.selected
                    } else {
                        true
                    };

                selected_tile.x = tile_button.x;
                selected_tile.y = tile_button.y;
                selected_tile.tile_set = tile_button.tile_set.clone();

                println!("pressed button with: {selected_tile:?}");
            }
            Interaction::Hovered => {}
            Interaction::None => {}
        }
    }
}

pub fn setup_music_system(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.spawn((
        AudioPlayer::new(asset_server.load("music/where_is_he.wav")),
        PlaybackSettings {
            mode: PlaybackMode::Loop,
            ..default()
        },
    ));
}

pub fn move_camera_to_player_character_system(
    mut camera_transform: Query<&mut Transform, (With<Camera>, Without<PlayerCharacter>)>,
    target: Query<&Transform, With<PlayerCharacter>>,
) {
    let mut camera_transform = camera_transform.single_mut();
    if let Ok(target) = target.get_single() {
        let target = target.translation.round();
        camera_transform.translation = target;
    }
}

pub fn move_camera_by_keyboard_system(
    mut input_event: EventReader<InputEvent>,
    mut camera_transform: Query<&mut Transform, With<Camera>>,
) {
    use crate::input::{Direction, Input};
    let mut camera_transform = camera_transform.single_mut();
    for event in input_event.read() {
        for input in &event.inputs {
            match input {
                Input::Move(Direction::LEFT) => {
                    camera_transform.translation.x -= 5.0;
                }
                Input::Move(Direction::RIGHT) => {
                    camera_transform.translation.x += 5.0;
                }
                Input::Move(Direction::UP) => {
                    camera_transform.translation.y += 5.0;
                }
                Input::Move(Direction::DOWN) => {
                    camera_transform.translation.y -= 5.0;
                }
                _ => {}
            }
        }
    }
}

pub fn setup_debug_output(mut commands: Commands) {
    // text
    let text = Text::new("Debug OUTPUT");
    commands.spawn((UI, DebugInfo::default(), text));
}

pub fn debug_output(
    mut debug_output: Query<(&mut Text, &mut DebugInfo), With<UI>>,
    diagnostics: Res<DiagnosticsStore>,
) {
    let fps = diagnostics
        .get(&FrameTimeDiagnosticsPlugin::FPS)
        .and_then(|fps| fps.smoothed());

    for (mut output, mut debug) in &mut debug_output {
        let mut map = BTreeMap::new();
        for (k, v) in &debug.text {
            if let Some(old) = map.get(k) {
                let new = format!("{old}, {v}");
                map.insert(k, new);
            } else {
                map.insert(k, v.to_string());
            }
        }
        output.0 = map.iter().map(|(k, v)| format!(" {k}: {v}\n")).collect();
        if let Some(fps) = fps {
            output.0 += &format!("{:3.1}", fps);
        }
        debug.text.clear();
    }
}
