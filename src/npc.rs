use crate::assets::{create_cut_meshes, Animation};
use crate::combat::combatant::Combatant;
use crate::combat::projectiles::FireProjectileEvent;
use crate::combat::status::{
    spawn_status_bar, StatusBarTyp, StatusValue, STATUS_HEIGHT_NPC, STATUS_WIDTH_NPC,
};
use crate::combat::{Item, Loot, RespawnIntervals};
use crate::hero::PlayerCharacter;
use crate::input::Direction;
use bevy::asset::{AssetServer, Assets};
use bevy::math::Vec3;
use bevy::prelude::*;
use rand::distributions::Distribution;
use rand::distributions::WeightedIndex;
use rand::prelude::SliceRandom;
use rand::Rng;
use std::time::Duration;

pub struct NpcPlugin;

impl Plugin for NpcPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup_hunter)
            .add_systems(FixedUpdate, (respawn_hunter, update_hunter_position));
    }
}

#[derive(Component)]
pub struct NPC;

#[derive(Component)]
pub struct HunterAI {
    last_direction: Direction,
    last_attack: Duration,
    last_moved: f32,
}

impl HunterAI {
    pub const RESPAWN_DURATION: f32 = 5.;
}

impl Default for HunterAI {
    fn default() -> Self {
        Self {
            last_direction: Direction::RIGHT,
            last_attack: Duration::from_millis(0),
            last_moved: 0.,
        }
    }
}

fn prepare_hunter_loot() -> Loot {
    let mut extra_dust = 0;
    let mut chance = 5;
    let mut rng = rand::thread_rng();
    while rng.gen_range(0..=5 + chance) <= chance {
        chance -= 1;
        extra_dust += 1;
    }

    let mut loot = Loot {
        is_taken: false,
        loot: Vec::with_capacity(2 + extra_dust),
    };
    loot.loot.push(Item::ManaDust);
    loot.loot.push(Item::ManaDust);
    for _ in 0..extra_dust {
        loot.loot.push(Item::ManaDust);
    }

    loot
}

fn setup_hunter(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    asset_server: Res<AssetServer>,
) {
    // main character
    let hero_texture_handle = asset_server.load("textures/AnimationSheet_Character.png");
    let hero_meshes_raw = create_cut_meshes(8, 9, &mut meshes);

    let mut animation = Animation::new(0., Direction::UP);
    for i in 0..8 {
        // TODO: use directions
        animation.add(None, 100, hero_meshes_raw[i][3].clone());
    }

    for i in 0..5 {
        let loot = prepare_hunter_loot();
        let npc = commands
            .spawn((
                NPC,
                HunterAI::default(),
                Combatant {
                    hp: StatusValue::new(50., 50.),
                    mp: None,
                    stamina: None,
                    respawn_name: "HunterAI".to_string(),
                    respawn_duration: HunterAI::RESPAWN_DURATION,
                    effects: vec![],
                    last_status_update: 0.0,
                },
                animation.clone(),
                Mesh2d(hero_meshes_raw[0][0].clone().into()),
                MeshMaterial2d(materials.add(ColorMaterial::from(hero_texture_handle.clone()))),
                Transform::from_translation(Vec3::new(-250. + 50. * i as f32, 150., 1.))
                    .with_scale(Vec3::splat(64.)),
                loot,
            ))
            .id();

        let full_mesh = Rectangle::new(STATUS_WIDTH_NPC.0, STATUS_HEIGHT_NPC).mesh();
        let position_hp = Transform::from_translation(Vec3::new(0., 0., 100.));
        spawn_status_bar(
            &mut commands,
            StatusBarTyp::HP,
            &mut meshes,
            &mut materials,
            npc,
            full_mesh,
            position_hp,
            STATUS_WIDTH_NPC.0,
        );
    }
}

pub fn hunter_attack(
    mut fire_projectile: EventWriter<FireProjectileEvent>,
    mut hunters: Query<(Entity, &Transform, &mut HunterAI)>,
    time: Res<Time<Virtual>>,
) {
    let now = time.elapsed();
    let mut rng = rand::thread_rng();
    for (entity, transform, mut hunter) in &mut hunters {
        if time.is_paused() {
            continue;
        }
        let mut transform = transform.clone();

        if hunter.last_attack + Duration::from_secs(1) <= now && rng.gen::<f32>() < 0.01 {
            hunter.last_attack = now;

            match hunter.last_direction {
                Direction::UP => transform.translation.y += 64.,
                Direction::DOWN => transform.translation.y -= 64.,
                Direction::LEFT => transform.translation.x -= 64.,
                Direction::RIGHT => transform.translation.x += 64.,
            };

            fire_projectile.send(FireProjectileEvent {
                spell: "explosion".to_string(),
                power: (rng.gen_range(1..=10) + rng.gen_range(1..=10)) as f32,
                source: entity,
                transform,
                direction: hunter.last_direction,
            });
        }
    }
}

fn respawn_hunter(
    hunters: Query<&HunterAI>,
    mut respawn_intervals: ResMut<RespawnIntervals>,
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    asset_server: Res<AssetServer>,
    time: Res<Time<Virtual>>,
) {
    let now = time.elapsed_secs();
    if !respawn_intervals.can_respawn("HunterAI", now) {
        return;
    }

    let hunter_count = hunters.iter().count();
    if hunter_count >= 5 {
        return;
    }

    respawn_intervals.set_respawn("HunterAI", now, HunterAI::RESPAWN_DURATION);
    // main character
    let texture_handle = asset_server.load("textures/AnimationSheet_Character.png");
    let meshes_raw = create_cut_meshes(8, 9, &mut meshes);

    let mut animation = Animation::new(0., Direction::UP);
    for i in 0..8 {
        // TODO: use directions
        animation.add(None, 100, meshes_raw[i][3].clone());
    }

    let loot = prepare_hunter_loot();
    let npc = commands
        .spawn((
            NPC,
            HunterAI {
                last_moved: now,
                ..default()
            },
            Combatant {
                hp: StatusValue::new(50., 50.),
                mp: None,
                stamina: None,
                respawn_name: "HunterAI".to_string(),
                respawn_duration: HunterAI::RESPAWN_DURATION,
                effects: vec![],
                last_status_update: 0.0,
            },
            animation.clone(),
            Mesh2d(meshes_raw[0][0].clone().into()),
            MeshMaterial2d(materials.add(ColorMaterial::from(texture_handle.clone()))),
            Transform::from_translation(Vec3::new(-250., 150., 1.)).with_scale(Vec3::splat(64.)),
            loot,
        ))
        .id();

    let full_mesh = Rectangle::new(STATUS_WIDTH_NPC.0, STATUS_HEIGHT_NPC).mesh();
    let position_hp = Transform::from_translation(Vec3::new(0., 0., 100.));
    spawn_status_bar(
        &mut commands,
        StatusBarTyp::HP,
        &mut meshes,
        &mut materials,
        npc,
        full_mesh,
        position_hp,
        STATUS_WIDTH_NPC.0,
    );
}

fn update_hunter_position(
    mut position: Query<(&mut Transform, &mut HunterAI), Without<PlayerCharacter>>,
    targets: Query<&Transform, With<PlayerCharacter>>,
    time: Res<Time<Virtual>>,
) {
    const SPEED: f32 = 60.;
    let targets: Vec<_> = targets.iter().collect();
    if targets.is_empty() {
        return;
    }
    let now = time.elapsed_secs();
    let mut rng = rand::thread_rng();

    for (mut transform, mut ai) in &mut position {
        const RANDOM_DIRECTION: usize = 5;
        const TOWARDS_PLAYER: usize = 10;
        const KEEP_LAST_DIRECTION: usize = 1000;
        let distribution =
            WeightedIndex::new([RANDOM_DIRECTION, TOWARDS_PLAYER, KEEP_LAST_DIRECTION]).unwrap();

        let next = match distribution.sample(&mut rng) {
            0 => Direction::rnd(&mut rng),
            1 => {
                let target = *(targets.choose(&mut rng).unwrap());
                let x = target.translation.x - transform.translation.x;
                let y = target.translation.y - transform.translation.y;

                if x.abs() > y.abs() {
                    if x > 0. {
                        Direction::RIGHT
                    } else {
                        Direction::LEFT
                    }
                } else {
                    if y > 0. {
                        Direction::UP
                    } else {
                        Direction::DOWN
                    }
                }
            }
            _ => ai.last_direction,
        };

        if (ai.last_direction == Direction::LEFT || next == Direction::LEFT)
            && (ai.last_direction != Direction::LEFT || next != Direction::LEFT)
        {
            transform.scale.x *= -1.0;
        }

        let duration = now - ai.last_moved;
        ai.last_moved = now;

        ai.last_direction = next;
        let distance = SPEED * duration;
        match next {
            Direction::RIGHT => transform.translation.x += distance,
            Direction::LEFT => transform.translation.x -= distance,
            Direction::UP => transform.translation.y += distance,
            Direction::DOWN => transform.translation.y -= distance,
        }
    }
}
