use std::collections::{BTreeMap, HashMap};
use std::sync::atomic::{AtomicI32, Ordering};

use bevy::asset::{AssetServer, Assets};
use bevy::math::Vec3;
use bevy::prelude::*;
use bevy::prelude::{ColorMaterial, Commands, Mesh, Res, ResMut, Transform};
use serde::{Deserialize, Serialize};

use crate::assets::create_cut_meshes;
use crate::constants::GRID_SIZE;
use crate::ldtk_import;

pub struct TileSetData {
    pub texture: Handle<Image>,
    pub mesh: Vec<Vec<Handle<Mesh>>>,
    pub image_path: String,
    pub count_height: usize,
    pub count_width: usize,
}

#[derive(Debug)]
pub enum Buildings {
    FoodConverter { position: [f32; 2] },
    ManaTree { position: [f32; 2] },
}

#[derive(Resource, Default)]
pub struct MapData {
    pub tile_sets: HashMap<String, TileSetData>,
    pub hero_spawn: [f32; 2],
    pub buildings: Vec<Buildings>,
}

pub fn setup_tiles(
    mut commands: &mut Commands,
    assets_meshes: &mut ResMut<Assets<Mesh>>,
    materials: &mut ResMut<Assets<ColorMaterial>>,
    asset_server: &Res<AssetServer>,
    map_data: &mut ResMut<MapData>,
    tile_spacing: i32,
) {
    use ldtk_import::import;

    let map = import("assets/maps/base.ldtk").unwrap();

    let mut tileset_uid_to_name = HashMap::new();

    for tileset in &map.defs.tilesets {
        let path = &tileset.rel_path;
        let path = if path.starts_with("../textures/") {
            path.replace("../textures/", "textures/")
        } else {
            path.clone()
        };
        let w = tileset.__c_wid as usize;
        let h = tileset.__c_hei as usize;
        let id = tileset.identifier.clone();

        tileset_uid_to_name.insert(tileset.uid, id.clone());

        let tile_set = load_tileset(assets_meshes, asset_server, path, w, h);

        map_data.tile_sets.insert(id, tile_set);
    }

    struct EntityDef {
        r#type: String,
        uid: i64,
    }
    let mut entitie_defs = BTreeMap::new();
    for entity in &map.defs.entities {
        for def in &entity.field_defs {
            entitie_defs.insert(
                entity.uid,
                EntityDef {
                    r#type: def.identifier.clone(),
                    uid: entity.uid,
                },
            );
        }
    }
    let fallback = AtomicI32::default();

    for level in &map.levels {
        for layer in &level.layer_instances {
            match layer.__type.as_str() {
                "IntGrid" => {
                    let name = layer
                        .__tileset_def_uid
                        .map(|uid| tileset_uid_to_name[&uid].to_string())
                        .unwrap_or_else(|| {
                            format!("fallback_uid_{}", fallback.fetch_and(1, Ordering::Relaxed))
                        });

                    for tile in &layer.auto_layer_tiles {
                        let [texture_x, texture_y] = tile.src;
                        let [coord_x, coord_y] = tile.px;

                        let (grid_x, grid_y) = coord_to_grid_index(
                            Vec2::new(coord_x as f32, (coord_y * -1) as f32),
                            0,
                        );

                        let tile_set_data = &map_data.tile_sets[&name];
                        let texture = &tile_set_data.texture;
                        let meshes = &tile_set_data.mesh;

                        add_tile(
                            &mut commands,
                            meshes,
                            materials,
                            texture,
                            name.clone(),
                            (texture_x / 32) as usize,
                            (texture_y / 32) as usize,
                            grid_x,
                            grid_y,
                            tile_spacing,
                        );
                    }
                }
                "Entities" => {
                    for entity in &layer.entity_instances {
                        if let Some(def) = entitie_defs.get(&entity.def_uid) {
                            match def.r#type.as_str() {
                                "hero" => {
                                    map_data.hero_spawn =
                                        [entity.px[0] as f32, entity.px[1] as f32];
                                }
                                "food_converter" => {
                                    map_data.buildings.push(Buildings::FoodConverter {
                                        position: [entity.px[0] as f32, entity.px[1] as f32],
                                    });
                                }
                                "mana_tree" => {
                                    map_data.buildings.push(Buildings::ManaTree {
                                        position: [entity.px[0] as f32, entity.px[1] as f32],
                                    });
                                }
                                unknown => {
                                    unimplemented!("{unknown}")
                                }
                            }
                        }
                    }
                }
                err => {
                    unimplemented!("{err}");
                }
            };
        }
    }
}

pub fn load_tileset(
    assets_meshes: &mut ResMut<Assets<Mesh>>,
    asset_server: &Res<AssetServer>,
    image_path: String,
    w: usize,
    h: usize,
) -> TileSetData {
    // tiles
    let tiles_texture_handle = asset_server.load(&image_path);
    let tiles_meshes_raw = create_cut_meshes(w, h, assets_meshes);

    TileSetData {
        texture: tiles_texture_handle,
        mesh: tiles_meshes_raw,
        image_path,
        count_height: h,
        count_width: w,
    }
}

#[derive(Component, Debug)]
pub struct DrawnMapTile {
    pub tile_set: String,
    pub texture_x: usize,
    pub texture_y: usize,

    pub pos_x: i32,
    pub pos_y: i32,
}

pub fn add_tile(
    commands: &mut Commands,
    meshes_raw: &Vec<Vec<Handle<Mesh>>>,
    materials: &mut ResMut<Assets<ColorMaterial>>,
    texture_handle: &Handle<Image>,
    tile_set_name: String,
    x_text: usize,
    y_text: usize,
    x: i32,
    y: i32,
    tile_spacing: i32,
) {
    let coord_x = x as f32 * (tile_spacing as f32 + GRID_SIZE);
    let coord_y = y as f32 * (tile_spacing as f32 + GRID_SIZE);

    commands.spawn((
        DrawnMapTile {
            tile_set: tile_set_name,
            texture_x: x_text,
            texture_y: y_text,
            pos_x: x,
            pos_y: y,
        },
        Mesh2d(meshes_raw[x_text][y_text].clone().into()),
        MeshMaterial2d(materials.add(ColorMaterial::from(texture_handle.clone()))),
        Transform::from_translation(Vec3::new(coord_x, coord_y, 0.))
            .with_scale(Vec3::splat(GRID_SIZE)),
    ));
}

pub fn coord_to_grid_index(coord: Vec2, tile_spacing: i32) -> (i32, i32) {
    let tile_spacing = tile_spacing as f32;

    let x = (coord.x / (GRID_SIZE + tile_spacing)) as i32;
    let y = (coord.y / (GRID_SIZE + tile_spacing)) as i32;

    (x, y)
}

pub fn grid_index_to_coord(grid_x: i32, grid_y: i32) -> Vec2 {
    Vec2::new(grid_x as f32 * GRID_SIZE, grid_y as f32 * GRID_SIZE)
}

#[derive(Serialize, Deserialize)]
pub struct Tile {
    pub set: String,
    pub x_tex: usize,
    pub y_tex: usize,
    pub x_pos: i32,
    pub y_pos: i32,
}

#[derive(Serialize, Deserialize)]
pub struct TileSet {
    pub path: String,
    pub x: usize,
    pub y: usize,
}

#[derive(Serialize, Deserialize, Default)]
pub struct GameMap {
    pub tile_sets: HashMap<String, TileSet>,
    pub tiles: Vec<Tile>,
}

#[cfg(test)]
mod test {
    use bevy::math::Vec2;

    use crate::game_map::coord_to_grid_index;

    #[test]
    fn test_coord_to_grid_overshot() {
        let (x, y) = coord_to_grid_index(Vec2::new(110., 140.), 2);

        assert_eq!(x, 3);
        assert_eq!(y, 4);
    }

    #[test]
    fn test_coord_to_grid_exact() {
        let (x, y) = coord_to_grid_index(Vec2::new(102., 136.), 2);

        assert_eq!(x, 3);
        assert_eq!(y, 4);
    }
}
