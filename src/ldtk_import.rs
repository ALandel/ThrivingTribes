use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::path::Path;

pub fn import<P: AsRef<Path>>(path: P) -> Result<model::LDtkProject, Box<dyn Error>> {
    let mut json = String::new();
    File::open(path)?.read_to_string(&mut json)?;

    Ok(serde_json::from_str(&json)?)
}

#[test]
fn test_project_version() {
    let project = import("assets/maps/base.ldtk").unwrap();

    // to be updated / extended on demand
    assert_eq!("1.5.3", &project.json_version);
}

pub mod model {
    use serde::{Deserialize, Serialize};

    #[derive(Serialize, Deserialize)]
    pub struct FieldInstance {
        pub __identifier: String,
        pub __type: String,
        // pub __value: Option<_>,
        // pub __tile: Option<_>,
        #[serde(rename = "defUid")]
        pub def_uid: i64,
        // #[serde(rename = "realEditorValues")]
        // pub real_editor_values: Vec<_>,
    }

    #[derive(Serialize, Deserialize)]
    pub struct EntityInstance {
        pub __identifier: String,
        pub __grid: Vec<i64>,
        pub __pivot: Vec<i64>,
        // pub __tags: Vec<_>,
        // pub __tile: Option<_>,
        #[serde(rename = "__smartColor")]
        pub __smart_color: String,
        #[serde(rename = "__worldX")]
        pub __world_x: i64,
        #[serde(rename = "__worldY")]
        pub __world_y: i64,
        pub iid: String,
        pub width: i64,
        pub height: i64,
        #[serde(rename = "defUid")]
        pub def_uid: i64,
        pub px: [i64; 2],
        #[serde(rename = "fieldInstances")]
        pub field_instances: Vec<FieldInstance>,
    }

    #[derive(Serialize, Deserialize)]
    pub struct AutoLayerTile {
        pub px: [i64; 2],
        pub src: [i64; 2],
        pub f: i64,
        pub t: i64,
        pub d: [i64; 2],
        pub a: i64,
    }

    #[derive(Serialize, Deserialize)]
    pub struct LayerInstance {
        pub __identifier: String,
        pub __type: String,
        #[serde(rename = "__cWid")]
        pub __c_wid: i64,
        #[serde(rename = "__cHei")]
        pub __c_hei: i64,
        #[serde(rename = "__gridSize")]
        pub __grid_size: i64,
        pub __opacity: i64,
        #[serde(rename = "__pxTotalOffsetX")]
        pub __px_total_offset_x: i64,
        #[serde(rename = "__pxTotalOffsetY")]
        pub __px_total_offset_y: i64,
        #[serde(rename = "__tilesetDefUid")]
        pub __tileset_def_uid: Option<i64>,
        #[serde(rename = "__tilesetRelPath")]
        pub __tileset_rel_path: Option<String>,
        pub iid: String,
        #[serde(rename = "levelId")]
        pub level_id: i64,
        #[serde(rename = "layerDefUid")]
        pub layer_def_uid: i64,
        #[serde(rename = "pxOffsetX")]
        pub px_offset_x: i64,
        #[serde(rename = "pxOffsetY")]
        pub px_offset_y: i64,
        pub visible: bool,
        // #[serde(rename = "optionalRules")]
        // pub optional_rules: Vec<_>,
        #[serde(rename = "intGridCsv")]
        pub int_grid_csv: Vec<i64>,
        #[serde(rename = "autoLayerTiles")]
        pub auto_layer_tiles: Vec<AutoLayerTile>,
        pub seed: i64,
        // #[serde(rename = "overrideTilesetUid")]
        // pub override_tileset_uid: Option<_>,
        // #[serde(rename = "gridTiles")]
        // pub grid_tiles: Vec<_>,
        #[serde(rename = "entityInstances")]
        pub entity_instances: Vec<EntityInstance>,
    }

    #[derive(Serialize, Deserialize)]
    pub struct Level {
        pub identifier: String,
        pub iid: String,
        pub uid: i64,
        #[serde(rename = "worldX")]
        pub world_x: i64,
        #[serde(rename = "worldY")]
        pub world_y: i64,
        #[serde(rename = "worldDepth")]
        pub world_depth: i64,
        #[serde(rename = "pxWid")]
        pub px_wid: i64,
        #[serde(rename = "pxHei")]
        pub px_hei: i64,
        #[serde(rename = "__bgColor")]
        pub __bg_color: String,
        // #[serde(rename = "bgColor")]
        // pub bg_color: Option<_>,
        #[serde(rename = "useAutoIdentifier")]
        pub use_auto_identifier: bool,
        // #[serde(rename = "bgRelPath")]
        // pub bg_rel_path: Option<_>,
        // #[serde(rename = "bgPos")]
        // pub bg_pos: Option<_>,
        #[serde(rename = "bgPivotX")]
        pub bg_pivot_x: f64,
        #[serde(rename = "bgPivotY")]
        pub bg_pivot_y: f64,
        #[serde(rename = "__smartColor")]
        pub __smart_color: String,
        // #[serde(rename = "__bgPos")]
        // pub __bg_pos: Option<_>,
        // #[serde(rename = "externalRelPath")]
        // pub external_rel_path: Option<_>,
        // #[serde(rename = "fieldInstances")]
        // pub field_instances: Vec<_>,
        #[serde(rename = "layerInstances")]
        pub layer_instances: Vec<LayerInstance>,
        // pub __neighbours: Vec<_>,
    }

    #[derive(Serialize, Deserialize)]
    pub struct CachedPixelData {
        #[serde(rename = "opaqueTiles")]
        pub opaque_tiles: String,
        #[serde(rename = "averageColors")]
        pub average_colors: String,
    }

    #[derive(Serialize, Deserialize)]
    pub struct Tileset {
        #[serde(rename = "__cWid")]
        pub __c_wid: i64,
        #[serde(rename = "__cHei")]
        pub __c_hei: i64,
        pub identifier: String,
        pub uid: i64,
        #[serde(rename = "relPath")]
        pub rel_path: String,
        // #[serde(rename = "embedAtlas")]
        // pub embed_atlas: Option<_>,
        #[serde(rename = "pxWid")]
        pub px_wid: i64,
        #[serde(rename = "pxHei")]
        pub px_hei: i64,
        #[serde(rename = "tileGridSize")]
        pub tile_grid_size: i64,
        pub spacing: i64,
        pub padding: i64,
        pub tags: Vec<String>,
        #[serde(rename = "tagsSourceEnumUid")]
        pub tags_source_enum_uid: Option<i32>,
        // #[serde(rename = "enumTags")]
        // pub enum_tags: Vec<_>,
        // #[serde(rename = "customData")]
        // pub custom_data: Vec<_>,
        // #[serde(rename = "savedSelections")]
        // pub saved_selections: Vec<_>,
        #[serde(rename = "cachedPixelData")]
        pub cached_pixel_data: CachedPixelData,
    }

    #[derive(Serialize, Deserialize)]
    pub struct FieldDef {
        pub identifier: String,
        // pub doc: Option<_>,
        pub __type: String,
        pub uid: i64,
        #[serde(rename = "type")]
        pub r#type: String,
        #[serde(rename = "isArray")]
        pub is_array: bool,
        #[serde(rename = "canBeNull")]
        pub can_be_null: bool,
        // #[serde(rename = "arrayMinLength")]
        // pub array_min_length: Option<_>,
        // #[serde(rename = "arrayMaxLength")]
        // pub array_max_length: Option<_>,
        #[serde(rename = "editorDisplayMode")]
        pub editor_display_mode: String,
        #[serde(rename = "editorDisplayScale")]
        pub editor_display_scale: i64,
        #[serde(rename = "editorDisplayPos")]
        pub editor_display_pos: String,
        #[serde(rename = "editorLinkStyle")]
        pub editor_link_style: String,
        // #[serde(rename = "editorDisplayColor")]
        // pub editor_display_color: Option<_>,
        #[serde(rename = "editorAlwaysShow")]
        pub editor_always_show: bool,
        #[serde(rename = "editorShowInWorld")]
        pub editor_show_in_world: bool,
        #[serde(rename = "editorCutLongValues")]
        pub editor_cut_long_values: bool,
        // #[serde(rename = "editorTextSuffix")]
        // pub editor_text_suffix: Option<_>,
        // #[serde(rename = "editorTextPrefix")]
        // pub editor_text_prefix: Option<_>,
        #[serde(rename = "useForSmartColor")]
        pub use_for_smart_color: bool,
        // pub min: Option<_>,
        // pub max: Option<_>,
        // pub regex: Option<_>,
        // #[serde(rename = "acceptFileTypes")]
        // pub accept_file_types: Option<_>,
        // #[serde(rename = "defaultOverride")]
        // pub default_override: Option<_>,
        // #[serde(rename = "textLanguageMode")]
        // pub text_language_mode: Option<_>,
        #[serde(rename = "symmetricalRef")]
        pub symmetrical_ref: bool,
        #[serde(rename = "autoChainRef")]
        pub auto_chain_ref: bool,
        #[serde(rename = "allowOutOfLevelRef")]
        pub allow_out_of_level_ref: bool,
        #[serde(rename = "allowedRefs")]
        pub allowed_refs: String,
        // #[serde(rename = "allowedRefsEntityUid")]
        // pub allowed_refs_entity_uid: Option<_>,
        // #[serde(rename = "allowedRefTags")]
        // pub allowed_ref_tags: Vec<_>,
        // #[serde(rename = "tilesetUid")]
        // pub tileset_uid: Option<_>,
    }

    #[derive(Serialize, Deserialize)]
    pub struct Entitie {
        pub identifier: String,
        pub uid: i64,
        // pub tags: Vec<_>,
        #[serde(rename = "exportToToc")]
        pub export_to_toc: bool,
        // pub doc: Option<_>,
        pub width: i64,
        pub height: i64,
        #[serde(rename = "resizableX")]
        pub resizable_x: bool,
        #[serde(rename = "resizableY")]
        pub resizable_y: bool,
        // #[serde(rename = "minWidth")]
        // pub min_width: Option<_>,
        // #[serde(rename = "maxWidth")]
        // pub max_width: Option<_>,
        // #[serde(rename = "minHeight")]
        // pub min_height: Option<_>,
        // #[serde(rename = "maxHeight")]
        // pub max_height: Option<_>,
        #[serde(rename = "keepAspectRatio")]
        pub keep_aspect_ratio: bool,
        #[serde(rename = "tileOpacity")]
        pub tile_opacity: i64,
        #[serde(rename = "fillOpacity")]
        pub fill_opacity: i64,
        #[serde(rename = "lineOpacity")]
        pub line_opacity: i64,
        pub hollow: bool,
        pub color: String,
        #[serde(rename = "renderMode")]
        pub render_mode: String,
        #[serde(rename = "showName")]
        pub show_name: bool,
        // #[serde(rename = "tilesetId")]
        // pub tileset_id: Option<_>,
        #[serde(rename = "tileRenderMode")]
        pub tile_render_mode: String,
        // #[serde(rename = "tileRect")]
        // pub tile_rect: Option<_>,
        // #[serde(rename = "uiTileRect")]
        // pub ui_tile_rect: Option<_>,
        // #[serde(rename = "nineSliceBorders")]
        // pub nine_slice_borders: Vec<_>,
        #[serde(rename = "maxCount")]
        pub max_count: i64,
        #[serde(rename = "limitScope")]
        pub limit_scope: String,
        #[serde(rename = "limitBehavior")]
        pub limit_behavior: String,
        #[serde(rename = "pivotX")]
        pub pivot_x: i64,
        #[serde(rename = "pivotY")]
        pub pivot_y: i64,
        #[serde(rename = "fieldDefs")]
        pub field_defs: Vec<FieldDef>,
    }

    #[derive(Serialize, Deserialize)]
    pub struct Rule {
        pub uid: i64,
        pub active: bool,
        pub size: i64,
        pub alpha: i64,
        pub chance: i64,
        #[serde(rename = "breakOnMatch")]
        pub break_on_match: bool,
        pub pattern: Vec<i64>,
        #[serde(rename = "flipX")]
        pub flip_x: bool,
        #[serde(rename = "flipY")]
        pub flip_y: bool,
        #[serde(rename = "xModulo")]
        pub x_modulo: i64,
        #[serde(rename = "yModulo")]
        pub y_modulo: i64,
        #[serde(rename = "xOffset")]
        pub x_offset: i64,
        #[serde(rename = "yOffset")]
        pub y_offset: i64,
        #[serde(rename = "tileXOffset")]
        pub tile_xoffset: i64,
        #[serde(rename = "tileYOffset")]
        pub tile_yoffset: i64,
        #[serde(rename = "tileRandomXMin")]
        pub tile_random_xmin: i64,
        #[serde(rename = "tileRandomXMax")]
        pub tile_random_xmax: i64,
        #[serde(rename = "tileRandomYMin")]
        pub tile_random_ymin: i64,
        #[serde(rename = "tileRandomYMax")]
        pub tile_random_ymax: i64,
        pub checker: String,
        #[serde(rename = "tileMode")]
        pub tile_mode: String,
        #[serde(rename = "pivotX")]
        pub pivot_x: i64,
        #[serde(rename = "pivotY")]
        pub pivot_y: i64,
        #[serde(rename = "outOfBoundsValue")]
        pub out_of_bounds_value: i64,
        #[serde(rename = "perlinActive")]
        pub perlin_active: bool,
        #[serde(rename = "perlinSeed")]
        pub perlin_seed: i64,
        #[serde(rename = "perlinScale")]
        pub perlin_scale: f64,
        #[serde(rename = "perlinOctaves")]
        pub perlin_octaves: i64,
    }

    #[derive(Serialize, Deserialize)]
    pub struct AutoRuleGroup {
        pub uid: i64,
        pub name: String,
        // pub color: Option<_>,
        // pub icon: Option<_>,
        pub active: bool,
        #[serde(rename = "isOptional")]
        pub is_optional: bool,
        pub rules: Vec<Rule>,
        #[serde(rename = "usesWizard")]
        pub uses_wizard: bool,
    }

    #[derive(Serialize, Deserialize)]
    pub struct IntGridValue {
        pub value: i64,
        pub identifier: String,
        pub color: String,
        // pub tile: Option<_>,
        #[serde(rename = "groupUid")]
        pub group_uid: i64,
    }

    #[derive(Serialize, Deserialize)]
    pub struct Layer {
        pub __type: String,
        pub identifier: String,
        #[serde(rename = "type")]
        pub r#type: String,
        pub uid: i64,
        // pub doc: Option<_>,
        // #[serde(rename = "uiColor")]
        // pub ui_color: Option<_>,
        #[serde(rename = "gridSize")]
        pub grid_size: i64,
        #[serde(rename = "guideGridWid")]
        pub guide_grid_wid: i64,
        #[serde(rename = "guideGridHei")]
        pub guide_grid_hei: i64,
        #[serde(rename = "displayOpacity")]
        pub display_opacity: i64,
        #[serde(rename = "inactiveOpacity")]
        pub inactive_opacity: f32,
        #[serde(rename = "hideInList")]
        pub hide_in_list: bool,
        #[serde(rename = "hideFieldsWhenInactive")]
        pub hide_fields_when_inactive: bool,
        #[serde(rename = "canSelectWhenInactive")]
        pub can_select_when_inactive: bool,
        #[serde(rename = "renderInWorldView")]
        pub render_in_world_view: bool,
        #[serde(rename = "pxOffsetX")]
        pub px_offset_x: i64,
        #[serde(rename = "pxOffsetY")]
        pub px_offset_y: i64,
        #[serde(rename = "parallaxFactorX")]
        pub parallax_factor_x: i64,
        #[serde(rename = "parallaxFactorY")]
        pub parallax_factor_y: i64,
        #[serde(rename = "parallaxScaling")]
        pub parallax_scaling: bool,
        // #[serde(rename = "requiredTags")]
        // pub required_tags: Vec<_>,
        // #[serde(rename = "excludedTags")]
        // pub excluded_tags: Vec<_>,
        #[serde(rename = "intGridValues")]
        pub int_grid_values: Vec<IntGridValue>,
        // #[serde(rename = "intGridValuesGroups")]
        // pub int_grid_values_groups: Vec<_>,
        #[serde(rename = "autoRuleGroups")]
        pub auto_rule_groups: Vec<AutoRuleGroup>,
        // #[serde(rename = "autoSourceLayerDefUid")]
        // pub auto_source_layer_def_uid: Option<_>,
        #[serde(rename = "tilesetDefUid")]
        pub tileset_def_uid: Option<i64>,
        #[serde(rename = "tilePivotX")]
        pub tile_pivot_x: i64,
        #[serde(rename = "tilePivotY")]
        pub tile_pivot_y: i64,
    }

    #[derive(Serialize, Deserialize)]
    pub struct Defs {
        pub layers: Vec<Layer>,
        pub entities: Vec<Entitie>,
        pub tilesets: Vec<Tileset>,
        // pub enums: Vec<_>,
        // #[serde(rename = "externalEnums")]
        // pub external_enums: Vec<_>,
        // #[serde(rename = "levelFields")]
        // pub level_fields: Vec<_>,
    }

    #[derive(Serialize, Deserialize)]
    pub struct Header {
        #[serde(rename = "fileType")]
        pub file_type: String,
        pub app: String,
        pub doc: String,
        pub schema: String,
        #[serde(rename = "appAuthor")]
        pub app_author: String,
        #[serde(rename = "appVersion")]
        pub app_version: String,
        pub url: String,
    }

    #[derive(Serialize, Deserialize)]
    pub struct LDtkProject {
        #[serde(rename = "__header__")]
        pub __header: Header,
        pub iid: String,
        #[serde(rename = "jsonVersion")]
        pub json_version: String,
        #[serde(rename = "appBuildId")]
        pub app_build_id: i64,
        #[serde(rename = "nextUid")]
        pub next_uid: i64,
        #[serde(rename = "identifierStyle")]
        pub identifier_style: String,
        // pub toc: Vec<_>,
        #[serde(rename = "worldLayout")]
        pub world_layout: String,
        #[serde(rename = "worldGridWidth")]
        pub world_grid_width: i64,
        #[serde(rename = "worldGridHeight")]
        pub world_grid_height: i64,
        #[serde(rename = "defaultLevelWidth")]
        pub default_level_width: i64,
        #[serde(rename = "defaultLevelHeight")]
        pub default_level_height: i64,
        #[serde(rename = "defaultPivotX")]
        pub default_pivot_x: i64,
        #[serde(rename = "defaultPivotY")]
        pub default_pivot_y: i64,
        #[serde(rename = "defaultGridSize")]
        pub default_grid_size: i64,
        #[serde(rename = "defaultEntityWidth")]
        pub default_entity_width: i64,
        #[serde(rename = "defaultEntityHeight")]
        pub default_entity_height: i64,
        #[serde(rename = "bgColor")]
        pub bg_color: String,
        #[serde(rename = "defaultLevelBgColor")]
        pub default_level_bg_color: String,
        #[serde(rename = "minifyJson")]
        pub minify_json: bool,
        #[serde(rename = "externalLevels")]
        pub external_levels: bool,
        #[serde(rename = "exportTiled")]
        pub export_tiled: bool,
        #[serde(rename = "simplifiedExport")]
        pub simplified_export: bool,
        #[serde(rename = "imageExportMode")]
        pub image_export_mode: String,
        #[serde(rename = "exportLevelBg")]
        pub export_level_bg: bool,
        // #[serde(rename = "pngFilePattern")]
        // pub png_file_pattern: Option<_>,
        #[serde(rename = "backupOnSave")]
        pub backup_on_save: bool,
        #[serde(rename = "backupLimit")]
        pub backup_limit: i64,
        // #[serde(rename = "backupRelPath")]
        // pub backup_rel_path: Option<_>,
        #[serde(rename = "levelNamePattern")]
        pub level_name_pattern: String,
        // #[serde(rename = "tutorialDesc")]
        // pub tutorial_desc: Option<_>,
        // #[serde(rename = "customCommands")]
        // pub custom_commands: Vec<_>,
        // pub flags: Vec<_>,
        pub defs: Defs,
        pub levels: Vec<Level>,
        // pub worlds: Vec<_>,
        #[serde(rename = "dummyWorldIid")]
        pub dummy_world_iid: String,
    }
}
