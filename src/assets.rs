use crate::input::Direction;
use bevy::asset::{Assets, Handle};
use bevy::prelude::*;

pub fn create_cut_meshes(
    width: usize,
    height: usize,
    meshes: &mut ResMut<Assets<Mesh>>,
) -> Vec<Vec<Handle<Mesh>>> {
    let mut meshes_raw = Vec::with_capacity(width);

    let width_f = width as f32;
    let height_f = height as f32;

    for x in 0..width {
        meshes_raw.push(Vec::with_capacity(height));
        let sub_meshes = meshes_raw.last_mut().unwrap();
        let x = x as f32;
        for y in 0..height {
            let y = y as f32;
            let mut mesh = Mesh::from(Rectangle::new(1., 1.));

            let up = y / height_f;
            let down = (y + 1.) / height_f;
            let left = x / width_f;
            let right = (x + 1.) / width_f;

            let down_left = [left, down];
            let up_left = [left, up];
            let up_right = [right, up];
            let down_right = [right, down];

            mesh.insert_attribute(
                Mesh::ATTRIBUTE_UV_0,
                vec![up_right, up_left, down_left, down_right],
            );

            sub_meshes.push(meshes.add(mesh));
        }
    }

    meshes_raw
}

#[derive(Clone, PartialEq, Default)]
struct SimpleAnimation {
    meshes: Vec<(u32, Handle<Mesh>)>,
    animation_length_in_ms: u32,
}

#[derive(Clone, PartialEq, Default)]
struct DirectionalAnimation {
    up: SimpleAnimation,
    down: SimpleAnimation,
    left: SimpleAnimation,
    right: SimpleAnimation,
}

#[derive(Clone, PartialEq)]
enum AnimationStyle {
    None,
    SIMPLE(SimpleAnimation),
    DIRECTIONAL(DirectionalAnimation),
}

impl AnimationStyle {
    fn is_none(&self) -> bool {
        &Self::None == self
    }

    fn unwrap_simple(&mut self) -> &mut SimpleAnimation {
        if let Self::SIMPLE(simple) = self {
            simple
        } else {
            panic!("requested simple animation")
        }
    }

    fn unwrap_directional(&mut self) -> &mut DirectionalAnimation {
        if let Self::DIRECTIONAL(directional) = self {
            directional
        } else {
            panic!("requested directional animation")
        }
    }
}

#[derive(Component, Clone)]
pub struct Animation {
    meshs: AnimationStyle,
    direction: Direction,
    offset: f32,
}

impl Animation {
    pub fn new(offset: f32, direction: Direction) -> Animation {
        Animation {
            meshs: AnimationStyle::None,
            direction,
            offset,
        }
    }

    fn mash_for_frame(&self, current: &f32) -> Handle<Mesh> {
        match &self.meshs {
            AnimationStyle::None => {}
            AnimationStyle::SIMPLE(animation) => {
                let mut frame =
                    ((*current - self.offset) * 1000.) as u32 % animation.animation_length_in_ms;
                for (duration, mesh) in &animation.meshes {
                    if frame < *duration {
                        return mesh.clone();
                    }
                    frame -= duration;
                }
            }
            AnimationStyle::DIRECTIONAL(DirectionalAnimation {
                left,
                right,
                up,
                down,
            }) => {
                let animation = match &self.direction {
                    Direction::UP => up,
                    Direction::DOWN => down,
                    Direction::LEFT => left,
                    Direction::RIGHT => right,
                };
                let mut frame =
                    ((*current - self.offset) * 1000.) as u32 % animation.animation_length_in_ms;

                for (duration, mesh) in &animation.meshes {
                    if frame < *duration {
                        return mesh.clone();
                    }
                    frame -= duration;
                }
            }
        }
        unreachable!()
    }

    pub fn add(&mut self, direction: Option<Direction>, duration_in_ms: u32, mesh: Handle<Mesh>) {
        use Direction::*;

        if self.meshs.is_none() {
            if direction.is_none() {
                self.meshs = AnimationStyle::SIMPLE(SimpleAnimation::default());
            } else {
                self.meshs = AnimationStyle::DIRECTIONAL(DirectionalAnimation::default())
            }
        }

        let simple_animation = match direction {
            None => self.meshs.unwrap_simple(),
            Some(direction) => {
                let animations = self.meshs.unwrap_directional();
                match direction {
                    UP => &mut animations.up,
                    DOWN => &mut animations.down,
                    LEFT => &mut animations.left,
                    RIGHT => &mut animations.right,
                }
            }
        };
        simple_animation.meshes.push((duration_in_ms, mesh));
        simple_animation.animation_length_in_ms += duration_in_ms;
    }
}

pub fn update_animation(
    time: Res<Time<Virtual>>,
    mut material_mesh: Query<(&Animation, &mut Mesh2d)>,
) {
    let now = time.elapsed_secs();
    for (animation, mut handle) in &mut material_mesh {
        handle.0 = animation.mash_for_frame(&now);
    }
}
