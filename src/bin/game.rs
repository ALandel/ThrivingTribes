use bevy::app::App;
use ThrivingTribes::ThrivingTribes;
use clap::Parser;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(long, default_value_t = false)]
    debug_output: bool,
    #[arg(long, default_value_t = false)]
    show_hitbox: bool,
}

pub fn main() {
    let Args {
        debug_output,
        show_hitbox
    } = Args::parse();

    App::new()
        .add_plugins(ThrivingTribes {
            debug_output,
            show_hitbox,
        })
        .run();
}
