use std::collections::BTreeMap;

use crate::combat::combatant::Combatant;
use bevy::app::App;
use bevy::prelude::*;

use crate::combat::Item;
use crate::hero::{PlayerCharacter, StatusEffect};
use crate::input::InputEvent;
use crate::model::UI;

pub struct EquipmentPlugin;

impl Plugin for EquipmentPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<UseItemEvent>()
            .add_systems(FixedUpdate, (check_use_item, use_item_handler))
            .add_systems(Update, print_items);
    }
}

#[derive(Component)]
pub struct Inventory {
    pub items: Vec<Item>,
}

impl Inventory {
    pub fn has(&self, item: &Item, count: usize) -> bool {
        let actual = self.items.iter().filter(|i| *i == item).count();

        actual >= count
    }

    pub fn add(&mut self, item: Item, count: usize) {
        for _ in 0..count {
            self.items.push(item.clone());
        }
    }

    pub fn remove(&mut self, item: &Item, count: usize) {
        for _ in 0..count {
            let mut i = 0;
            let mut abourt = true;
            for (index, current) in self.items.iter().enumerate() {
                if *current == *item {
                    i = index;
                    abourt = false;
                    break;
                }
            }

            if abourt {
                return;
            }

            self.items.remove(i);
        }
    }
}

#[derive(Event)]
struct UseItemEvent {
    item: Item,
}

fn check_use_item(
    mut event_reader: EventReader<InputEvent>,
    mut event_writer: EventWriter<UseItemEvent>,
) {
    use crate::input::Input;

    let mut use_item = false;
    for event in event_reader.read() {
        if event.inputs.contains(&Input::UseItem) {
            use_item = true;
        }
    }

    if use_item {
        event_writer.send(UseItemEvent {
            item: Item::ManaPotion,
        });
    }
}

fn use_item_handler(
    mut event_reader: EventReader<UseItemEvent>,
    mut player_character: Query<(&mut Combatant, &mut Inventory), With<PlayerCharacter>>,
) {
    for event in event_reader.read() {
        for (mut player, mut inventory) in &mut player_character {
            let item = &event.item;
            if inventory.has(item, 1) {
                inventory.remove(item, 1);

                if *item == Item::ManaPotion {
                    player.add_effect(StatusEffect::ManaRegeneration {
                        amount: 200.,
                        added_factor: 4.,
                    });
                }
            }
        }
    }
}

#[derive(Component)]
struct InventarText;

fn print_items(
    player: Query<&Inventory, With<PlayerCharacter>>,
    old_texts: Query<Entity, With<InventarText>>,
    mut commands: Commands,
) {
    for old in &old_texts {
        commands.entity(old).despawn();
    }

    let player = match player.get_single() {
        Ok(player) => player,
        Err(err) => {
            eprintln!("in print_items: {err}");
            return;
        }
    };

    let mut items = BTreeMap::new();
    for item in &player.items {
        let name = format!("{item:?}");
        if let Some(val) = items.get_mut(&name) {
            *val += 1;
        } else {
            items.insert(name, 1);
        }
    }

    let mut new_texts_raw = Vec::with_capacity(items.len());
    for (item, count) in &items {
        new_texts_raw.push(format!("{item:10}: {count:3}"));
    }
    new_texts_raw.sort();

    let text = new_texts_raw.join("\n");
    let text = Text(text);
    commands.spawn((UI, InventarText, text));
}
