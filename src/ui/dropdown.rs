use bevy::prelude::*;

#[derive(Component)]
pub struct Dropdown {
    button: Entity,
    items: Vec<String>,
    open: bool,
}

pub fn create_dropdown(
    text: String,
    options: Vec<String>,
    commands: &mut Commands,
    // asset_server: &Res<AssetServer>,
) -> Entity {
    println!("dropdown created");

    // let node = NodeBundle {
    //     style: Style {
    //         top: Val::Percent(0.),
    //         left: Val::Percent(0.),
    // width: Val::Px(200.),
    // height: Val::Auto,
    // ..default()
    // },
    // ..default()
    // };
    // let text = create_text_bundle(text, &asset_server);

    let text_id = commands.spawn(Text::new(text)).id();
    let mut button = commands.spawn(Button);
    button.add_child(text_id);
    let button = button.id();

    // let node = commands.spawn(node).id();
    // commands.entity(node).add_child(button);

    let dropdown = Dropdown {
        button,
        items: options,
        open: false,
    };
    commands.entity(button).insert(dropdown);

    button
}

pub fn open_dropdown(
    interaction_query: Query<(Entity, &Interaction, &Dropdown), Changed<Interaction>>,
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    for (entity, interaction, dropdown) in &interaction_query {
        if let Interaction::Pressed = interaction {
            for (i, text) in dropdown.items.iter().enumerate() {
                let node = Node::DEFAULT;
                // NodeBundle {
                // style: Style {
                // left: Val::Px(0 as f32),
                // top: Val::Px(20. + 20. * i as f32),
                // width: Val::Percent(100.),
                // position_type: PositionType::Absolute,
                // ..default()
                // },
                // ..default()
                // };
                let node = commands.spawn(node).id();
                let button = commands.spawn(Button).id();
                let text = commands.spawn(Text::new(text)).id();
                commands.entity(entity).add_child(node);
                commands.entity(node).add_child(button);
                // commands.entity(parent.get()).add_child(node);
                commands.entity(button).add_child(text);
            }
        }
    }
}

pub fn interact_with_dropdown(
    mut interaction_query: Query<(&Interaction, &mut Visibility), Changed<Interaction>>,
    mut dropdown_query: Query<&mut Visibility, With<Dropdown>>,
) {
    println!("dropdown triggered");
    for (interaction, mut visibility) in interaction_query.iter_mut() {
        match interaction {
            Interaction::Pressed => {
                // Wenn der Benutzer auf das Dropdown-Menü klickt, ändern wir den Status auf "Visible"
                *visibility = Visibility::Visible;
            }
            Interaction::None => {
                // Wenn der Benutzer außerhalb des Dropdown-Menüs klickt, ändern wir den Status auf "Hidden"
                *visibility = Visibility::Hidden;
            }
            _ => {}
        }
    }

    // Aktualisieren Sie die Sichtbarkeit aller Dropdown-Menüs basierend auf ihrem aktuellen Status
    for visibility in dropdown_query.iter_mut() {
        if *visibility == Visibility::Visible {
            // Wenn das Dropdown-Menü sichtbar ist, führen Sie hier Aktionen durch
        } else {
            // Wenn das Dropdown-Menü nicht sichtbar ist, führen Sie hier andere Aktionen durch
        }
    }
}
