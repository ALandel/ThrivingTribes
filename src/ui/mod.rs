use crate::ui::dropdown::open_dropdown;
use bevy::app::App;
use bevy::asset::AssetServer;
use bevy::prelude::*;

mod dropdown;
pub mod mainmenu;

pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, open_dropdown);
    }
}
