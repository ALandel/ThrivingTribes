use bevy::app::{App, Plugin, Update};
use bevy::asset::AssetServer;
use bevy::prelude::*;

use crate::input::{Input, InputEvent};
use crate::model::UI;
use crate::ui::dropdown::create_dropdown;

pub struct Menu;

impl Plugin for Menu {
    fn build(&self, app: &mut App) {
        app.add_event::<TriggerMainMenuEvent>()
            .add_systems(Update, (trigger_mainmenu, mainmenu_trigger_handler));
    }
}

#[derive(Event)]
pub struct TriggerMainMenuEvent;

pub fn trigger_mainmenu(
    mut event_reader: EventReader<InputEvent>,
    mut event_writer: EventWriter<TriggerMainMenuEvent>,
) {
    let mut send_triger = false;
    for event in event_reader.read() {
        for input in &event.inputs {
            if *input == Input::MainMenu {
                warn!("triggering main menu");
                send_triger = true;
                break;
            }
        }
    }

    if send_triger {
        event_writer.send(TriggerMainMenuEvent);
    }
}

#[derive(Component)]
pub struct MainMenu;

pub fn mainmenu_trigger_handler(
    mut events: EventReader<TriggerMainMenuEvent>,
    main_menu: Query<Entity, With<MainMenu>>,
    mut commands: Commands,
    mut time: ResMut<Time<Virtual>>,
) {
    let mut switch = false;
    for _ in events.read() {
        switch = true;
    }

    if switch {
        if let Ok(entity) = main_menu.get_single() {
            warn!("despawn main menu");
            time.unpause();
            commands.entity(entity).despawn();
        } else {
            warn!("spawn main menu");
            time.pause();

            let dropdown = create_dropdown(
                "Thriving Tribes - Dropdown".to_string(),
                vec![
                    "option 1".to_string(),
                    "option b".to_string(),
                    "option III".to_string(),
                ],
                &mut commands,
            );

            commands
                .spawn((
                    UI,
                    MainMenu,
                    // NodeBundle {
                    // style: Style {
                    // top: Val::Px(100.),
                    // left: Val::Px(200.),
                    // ..default()
                    // },
                    // ..default()
                    // },
                ))
                .add_child(dropdown);
        }
    }
}
