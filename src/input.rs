use rand::prelude::*;
use bevy::prelude::*;
use rand::Rng;
use std::collections::{BTreeMap, BTreeSet};
use crate::input::Direction::{DOWN, LEFT, RIGHT, UP};

pub struct InputPlugin;

impl Plugin for InputPlugin {
    fn build(&self, app: &mut App) {
        app
            .add_event::<InputEvent>()
            .add_systems(FixedUpdate, input_handler)
        ;
    }
}


#[derive(Clone, PartialEq, PartialOrd, Eq, Ord)]
pub enum Input {
    Move(Direction),
    Fire(&'static str),
    MainMenu,
    Use,
    UseItem,
}

#[derive(Event)]
pub struct InputEvent {
    pub inputs: BTreeSet<Input>,
}

#[derive(Clone, Copy, PartialEq, PartialOrd, Eq, Ord, Debug)]
pub enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT,
}

fn input_handler(
    keyboard: Res<bevy::input::ButtonInput<KeyCode>>,
    mut event_writer: EventWriter<InputEvent>,
) {
    let mut key_code_to_input = BTreeMap::new();
    key_code_to_input.insert(KeyCode::ArrowLeft, (true, Input::Move(LEFT)));
    key_code_to_input.insert(KeyCode::KeyA, (true, Input::Move(LEFT)));

    key_code_to_input.insert(KeyCode::ArrowRight, (true, Input::Move(RIGHT)));
    key_code_to_input.insert(KeyCode::KeyD, (true, Input::Move(RIGHT)));

    key_code_to_input.insert(KeyCode::ArrowUp, (true, Input::Move(UP)));
    key_code_to_input.insert(KeyCode::KeyW, (true, Input::Move(UP)));

    key_code_to_input.insert(KeyCode::ArrowDown, (true, Input::Move(DOWN)));
    key_code_to_input.insert(KeyCode::KeyS, (true, Input::Move(DOWN)));

    key_code_to_input.insert(KeyCode::Space, (true, Input::Fire("explosion")));
    key_code_to_input.insert(KeyCode::ControlLeft, (true, Input::Fire("fireball")));

    key_code_to_input.insert(KeyCode::Escape, (false, Input::MainMenu));

    key_code_to_input.insert(KeyCode::KeyE, (false, Input::Use));

    key_code_to_input.insert(KeyCode::Digit1, (false, Input::UseItem));

    let mut inputs = keyboard.get_pressed()
        .filter_map(|key_code| key_code_to_input.get(key_code))
        .map(|input| input.to_owned())
        .filter(|(autofire, _)| *autofire)
        .map(|(_, input)| input)
        .collect::<BTreeSet<_>>();

    let mut new_inputs = keyboard.get_just_pressed()
        .filter_map(|key_code| key_code_to_input.get(key_code))
        .map(|input| input.to_owned())
        .filter(|(autofire, _)| !autofire)
        .map(|(_, input)| input)
        .collect::<BTreeSet<_>>();

    inputs.append(&mut new_inputs);

    let event = InputEvent {
        inputs
    };

    event_writer.send(event);
}


impl Direction {
    pub fn rnd(rnd: &mut ThreadRng) -> Direction {
        match rnd.gen_range(0..4) {
            0 => Direction::UP,
            1 => Direction::DOWN,
            2 => Direction::LEFT,
            _ => Direction::RIGHT
        }
    }
}
