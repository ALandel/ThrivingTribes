use bevy::app::{App, Plugin, Startup, Update};
use bevy::asset::{Assets, AssetServer};
use bevy::DefaultPlugins;
use bevy::prelude::*;

use crate::assets::update_animation;
use crate::buildings::BuildingsPlugin;
use crate::combat::Combat;
use crate::debug::DebugPlugin;
use crate::equipment::EquipmentPlugin;
use crate::game_map::{MapData, setup_tiles};
use crate::hero::HeroPlugin;
use crate::input::InputPlugin;
use crate::npc::NpcPlugin;
use crate::systems::setup_music_system;
use crate::ui::mainmenu::Menu;
use crate::ui::UiPlugin;

pub mod model;
pub mod systems;
mod game_map;
mod assets;
mod constants;
mod hero;
mod npc;
mod combat;
mod ldtk_import;
mod input;
mod debug;
pub mod ui;
mod buildings;
mod equipment;

pub struct ThrivingTribes {
    pub debug_output: bool,
    pub show_hitbox: bool,
}

impl Plugin for ThrivingTribes {
    fn build(&self, app: &mut App) {
        app
            .add_plugins(DefaultPlugins.set(
                ImagePlugin::default_nearest(),
            ))
            .insert_resource(Time::<Fixed>::from_seconds(1. / 25.))
            .add_systems(Startup, setup_camara)
            .init_resource::<MapData>()
            .add_systems(Startup, setup_game)
            .add_systems(Startup, setup_music_system)
            .add_plugins(InputPlugin)
            // .add_systems(Update, (draw_funny_stuff, update_config))

            // .add_systems(Update, button_system)
            .add_systems(Update, update_animation)
            .add_plugins(HeroPlugin)
            .add_plugins(Combat {
                show_hitbox: self.show_hitbox
            })
            .add_plugins(NpcPlugin)
            .add_plugins(UiPlugin)
            .add_plugins(Menu)
            .add_plugins(BuildingsPlugin)
            .add_plugins(EquipmentPlugin)
        ;

        if self.debug_output {
            app.add_plugins(DebugPlugin);
        }
    }
}

fn setup_game(mut commands: Commands,
              mut meshes: ResMut<Assets<Mesh>>,
              mut materials: ResMut<Assets<ColorMaterial>>,
              asset_server: Res<AssetServer>,
              mut map_data: ResMut<MapData>,
) {
    setup_tiles(&mut commands, &mut meshes, &mut materials, &asset_server, &mut map_data, 0);
}

fn setup_camara(mut commands: Commands, mut time: ResMut<Time<Virtual>>) {
    time.set_relative_speed(4.);
    // Spawn camera
    let mut camera = Camera2dBundle::default();
    camera.projection.scale = 2.;
    commands.spawn(camera);
}
