use std::collections::BTreeSet;
use std::time::Duration;

use bevy::asset::{AssetServer, Assets};
use bevy::math::Vec3;
use bevy::prelude::*;
use rand::Rng;

use crate::assets::{create_cut_meshes, Animation};
use crate::combat::combatant::Combatant;
use crate::combat::projectiles::FireProjectileEvent;
use crate::combat::spells::GlobalSpellBook;
use crate::combat::status::{StatusBar, StatusBarTyp, StatusValue, STATUS_WIDTH_PLAYER};
use crate::combat::{status, Item};
use crate::equipment::Inventory;
use crate::game_map::MapData;
use crate::input::Direction::{DOWN, LEFT, RIGHT, UP};
use crate::input::Input::Fire;
use crate::input::{Direction, Input, InputEvent};
use crate::setup_game;
use crate::systems::move_camera_to_player_character_system;

pub struct HeroPlugin;

impl Plugin for HeroPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, (setup_hero.after(setup_game),))
            .add_systems(FixedUpdate, (update_hero_position,))
            .add_systems(
                Update,
                (move_camera_to_player_character_system.after(update_hero_position),),
            );
    }
}

#[derive(Component, Debug)]
pub struct PlayerCharacter {
    last_direction: Direction,
    cool_down_end: Duration,
    mirrored: bool,
    last_moved: f32,
}

impl PlayerCharacter {
    pub fn is_in_cool_down(&self, now: Duration) -> bool {
        now < self.cool_down_end
    }

    pub fn start_cool_down(&mut self, now: Duration, duration_in_ms: u64) {
        self.cool_down_end = now + Duration::from_millis(duration_in_ms);
    }
}

#[derive(Debug)]
pub enum StatusEffect {
    ManaRegeneration { amount: f32, added_factor: f32 },
}

pub fn setup_hero(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    asset_server: Res<AssetServer>,
    map_data: Res<MapData>,
) {
    // main character
    let hero_texture_handle = asset_server.load("textures/AnimationSheet_Character.png");
    let hero_meshes_raw = create_cut_meshes(8, 9, &mut meshes);
    let x = map_data.hero_spawn[0];
    let y = map_data.hero_spawn[1];

    let mut animation = Animation::new(0., UP);
    // TODO: use directions
    animation.add(None, 250, hero_meshes_raw[0][0].clone());
    animation.add(None, 500, hero_meshes_raw[1][0].clone());

    let hero_scaling = Vec3::splat(64.);
    let hero = commands.spawn((
        PlayerCharacter {
            last_direction: RIGHT,
            cool_down_end: Duration::from_millis(0),
            mirrored: false,
            last_moved: 0.,
        },
        Inventory {
            items: vec![
                Item::ManaPotion,
                Item::ManaDust,
                Item::ManaDust,
                Item::ManaDust,
                Item::ManaDust,
                Item::ManaDust,
            ],
        },
        Combatant {
            hp: StatusValue::new(100., 100.),
            mp: Some(StatusValue::new(100., 100.)),
            stamina: Some(StatusValue::new(5000., 5000.)),
            respawn_name: "PlayerCharacter".to_string(),
            respawn_duration: 0.,
            effects: vec![],
            last_status_update: 0.0,
        },
        animation,
        Mesh2d(hero_meshes_raw[0][0].clone().into()),
        MeshMaterial2d(materials.add(ColorMaterial::from(hero_texture_handle.clone()))),
        Transform::from_translation(Vec3::new(x, -y, 1.)).with_scale(hero_scaling.clone()),
    ));
    let hero_id = hero.id();

    // let full_mesh = Rectangle::new(FULL, 10.).mesh();
    let full_mesh = Rectangle::new(STATUS_WIDTH_PLAYER.0, 10.).mesh();

    let position_hp = Transform::from_translation(Vec3::new(0., 0., 100.))
        // .with_scale(invert_scaling)
        ;
    // position_hp.translation.y = 1.;
    status::spawn_status_bar(
        &mut commands,
        StatusBarTyp::HP,
        &mut meshes,
        &mut materials,
        hero_id,
        full_mesh,
        position_hp,
        STATUS_WIDTH_PLAYER.0,
    );

    let position_mp = Transform::from_translation(Vec3::new(-(STATUS_WIDTH_PLAYER.1 / 2.), 0.8, 100.))
        // .with_scale(Vec3::splat(1. / 64.))
        ;
    // position_hp.translation.y = 1.;
    let half_mesh = Rectangle::new(STATUS_WIDTH_PLAYER.1, 8.).mesh();
    status::spawn_status_bar(
        &mut commands,
        StatusBarTyp::MP,
        &mut meshes,
        &mut materials,
        hero_id,
        half_mesh,
        position_mp,
        STATUS_WIDTH_PLAYER.1,
    );

    let half_mesh = Rectangle::new(STATUS_WIDTH_PLAYER.1, 8.).mesh();
    let position_stamina = Transform::from_translation(Vec3::new(STATUS_WIDTH_PLAYER.1 / 2., 0.8, 100.))
        // .with_scale(Vec3::splat(1. / 64.))
        ;
    status::spawn_status_bar(
        &mut commands,
        StatusBarTyp::STAMINA,
        &mut meshes,
        &mut materials,
        hero_id,
        half_mesh,
        position_stamina,
        STATUS_WIDTH_PLAYER.1,
    );
}

pub fn update_hero_position(
    mut event_reader: EventReader<InputEvent>,
    mut position: Query<(&mut Transform, &mut PlayerCharacter, &mut Combatant)>,
    mut status_bars: Query<&mut Transform, (With<StatusBar>, Without<PlayerCharacter>)>,
    time: Res<Time<Virtual>>,
) {
    let now = time.elapsed_secs();
    const BASE_SPEED: f32 = 100.0;
    const MOVE_COST: f32 = 1.;

    if let Ok((mut transform, mut character, mut combatant)) = position.get_single_mut() {
        let mut directions = read_directions(&mut event_reader);
        remove_oposing_directions(&mut directions);

        let duration = now - character.last_moved;
        character.last_moved = now;

        let directions_modifier = if directions.len() == 0 {
            return;
        } else if directions.len() == 1 {
            1.
        } else {
            1. / f32::sqrt(2.)
        };

        let mp_factor = if let Some(mp) = &mut combatant.mp {
            mp.fraction()
        } else {
            1.
        };
        let factor = mp_factor * duration;
        let move_cost = MOVE_COST * factor;

        let mut must_be_mirrored = false;
        let mut next = UP;

        if directions.contains(&UP) {
            must_be_mirrored = character.mirrored;
            transform.translation.y += BASE_SPEED * factor * directions_modifier;
            next = UP;
        }
        if directions.contains(&DOWN) {
            must_be_mirrored = character.mirrored;
            transform.translation.y -= BASE_SPEED * factor * directions_modifier;
            next = DOWN;
        }
        if directions.contains(&LEFT) {
            transform.translation.x -= BASE_SPEED * factor * directions_modifier;
            must_be_mirrored = true;
            next = LEFT;
        }
        if directions.contains(&RIGHT) {
            transform.translation.x += BASE_SPEED * factor * directions_modifier;
            must_be_mirrored = false;
            next = RIGHT;
        }

        if let Some(mp) = &mut combatant.mp {
            mp.current -= move_cost;
        }

        if character.mirrored != must_be_mirrored {
            transform.scale.x *= -1.0;
            character.mirrored = must_be_mirrored;

            for mut transform in &mut status_bars {
                transform.translation.x *= -1.0;
                transform.scale.x *= -1.0;
            }
        }

        character.last_direction = next;
    }
}

fn read_directions(event_reader: &mut EventReader<InputEvent>) -> BTreeSet<Direction> {
    let mut directions = BTreeSet::new();

    for event in event_reader.read() {
        for input in &event.inputs {
            if let Input::Move(direction) = input {
                directions.insert(*direction);
            }
        }
    }

    directions
}

fn remove_oposing_directions(directions: &mut BTreeSet<Direction>) {
    if directions.contains(&LEFT) && directions.contains(&RIGHT) {
        directions.remove(&LEFT);
        directions.remove(&RIGHT);
    }

    if directions.contains(&UP) && directions.contains(&DOWN) {
        directions.remove(&UP);
        directions.remove(&DOWN);
    }
}

pub fn hero_cast_spell(
    mut event_reader: EventReader<InputEvent>,
    mut fire_projectile: EventWriter<FireProjectileEvent>,
    mut player_character: Query<(Entity, &Transform, &mut PlayerCharacter, &mut Combatant)>,
    global_spell_book: Res<GlobalSpellBook>,
    time: Res<Time<Virtual>>,
) {
    let now = time.elapsed();
    for event in event_reader.read() {
        if time.is_paused() {
            continue;
        }
        if let Ok((entity, transform, mut player, mut combatant)) =
            player_character.get_single_mut()
        {
            if player.is_in_cool_down(now) {
                continue;
            }
            let mut spell_name = None;
            for input in &event.inputs {
                if let Fire(spell) = *input {
                    spell_name = Some(spell);
                    break;
                }
            }

            let spell_name = if let Some(name) = spell_name {
                name.to_owned()
            } else {
                continue;
            };

            let spell = if let Some(spell) = global_spell_book.spells.get(&spell_name) {
                spell
            } else {
                eprintln!("got invalid spell fire event with name: {spell_name}");
                continue;
            };

            let mp_cost = spell.cost as f32;
            if let Some(mp) = &combatant.mp {
                if mp.current < mp_cost {
                    return;
                }
            }

            if let Some(mp) = &mut combatant.mp {
                mp.current -= mp_cost;
            }

            let mut rng = rand::thread_rng();
            let mut power = spell.damage.base;
            for _ in 0..spell.damage.variant_count {
                power += rng.gen::<f32>() * spell.damage.variant;
            }

            player.start_cool_down(now, spell.cooldown_in_ms as u64);

            let mut transform = transform.clone();
            match player.last_direction {
                UP => transform.translation.y += 64.,
                DOWN => transform.translation.y -= 64.,
                LEFT => transform.translation.x -= 64.,
                RIGHT => transform.translation.x += 64.,
            };

            fire_projectile.send(FireProjectileEvent {
                spell: spell_name,
                power,
                source: entity,
                transform,
                direction: player.last_direction,
            });
        }
    }
}
