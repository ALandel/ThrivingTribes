use std::collections::HashMap;
use std::time::Duration;

use bevy::app::{App, Update};
use bevy::math::bounding::{Aabb2d, IntersectsVolume};
use bevy::math::Vec3;
use bevy::prelude::*;
use bevy::time::common_conditions::on_timer;
use combatant::spawn_loot_from_died_combatant;

use crate::combat::combatant::{
    combatant_died_sound, despawn_died_combatants, update_status, CombatantDied,
};
use crate::combat::projectiles::{
    check_projectile_hits, despawn_projectile, draw_hitboxes, fire_projectile_handler,
    fire_projectile_sound, projectile_hit_handler, projectile_hit_sound,
    update_projectile_position, FireProjectileEvent, ProjectileHitEvent,
};
use crate::combat::spells::{setup_global_spell_book, GlobalSpellBook};
use crate::combat::status::{draw_and_update_status_bars, print_status_effects};
use crate::equipment::Inventory;
use crate::hero::hero_cast_spell;
use crate::npc::hunter_attack;

pub mod combatant;
pub mod projectiles;
pub mod spells;
pub mod status;

pub struct Combat {
    pub show_hitbox: bool,
}

impl Plugin for Combat {
    fn build(&self, app: &mut App) {
        app.add_event::<FireProjectileEvent>()
            .add_event::<ProjectileHitEvent>()
            .add_event::<CombatantDied>()
            .add_event::<SpawnLootEvent>()
            .init_resource::<GlobalSpellBook>()
            .init_resource::<RespawnIntervals>()
            .add_systems(
                Update,
                (
                    (hero_cast_spell, hunter_attack),
                    fire_projectile_handler
                        .after(hero_cast_spell)
                        .after(hunter_attack),
                    fire_projectile_sound
                        .after(hero_cast_spell)
                        .after(hunter_attack),
                    despawn_projectile,
                    check_projectile_hits,
                    projectile_hit_handler.after(check_projectile_hits),
                    projectile_hit_sound.after(check_projectile_hits),
                    spawn_loot_from_died_combatant,
                    despawn_died_combatants.after(projectile_hit_handler),
                    combatant_died_sound.after(projectile_hit_handler),
                    update_projectile_position.run_if(on_timer(Duration::from_millis(1000 / 25))),
                ),
            )
            .add_systems(Startup, setup_global_spell_book)
            .add_systems(Update, update_status)
            .add_systems(Update, (draw_and_update_status_bars, print_status_effects))
            .add_event::<LootPickupEvent>()
            .add_systems(
                Update,
                (
                    spawn_loot_handler,
                    draw_loot,
                    pickup_loot_check,
                    pickup_loot_handler,
                    pickup_sound,
                ),
            );

        if self.show_hitbox {
            app.add_systems(Update, draw_hitboxes);
        }
    }
}

#[derive(Component, Clone)]
pub struct Loot {
    pub is_taken: bool,
    pub loot: Vec<Item>,
}

#[derive(Event)]
pub struct LootPickupEvent {
    pub loot: Entity,
    pub taker: Entity,
}

#[derive(Clone, PartialEq, Debug)]
pub enum Item {
    ManaDust,
    ManaPotion,
}

#[derive(Component)]
pub struct Position {
    pub x: f32,
    pub y: f32,
}

#[derive(Event)]
pub struct SpawnLootEvent {
    pub position: Vec2,
    pub loot: Loot,
}

fn spawn_loot_handler(mut commands: Commands, mut spawn_loot_event: EventReader<SpawnLootEvent>) {
    for event in spawn_loot_event.read() {
        if !event.loot.is_taken {
            let position = Position {
                x: event.position.x,
                y: event.position.y,
            };
            log::info!("Dropping {} items", event.loot.loot.len());

            commands.spawn((event.loot.to_owned(), position));
        } else {
            log::info!("No items dropped");
        }
    }
}

fn draw_loot(items: Query<(&Loot, &Position)>, mut gizmos: Gizmos, time: Res<Time<Virtual>>) {
    let now = time.elapsed();
    let now_in_ms = now.as_millis();
    for (_loot, position) in &items {
        gizmos.circle_2d(
            Vec2::new(position.x, position.y),
            4. + ((now_in_ms / 200) % 5) as f32,
            Color::hsl(240., 0.5, 0.5),
        );
    }
}

fn pickup_loot_check(
    mut entity_with_inventory: Query<(Entity, &Transform), With<Inventory>>,
    loot: Query<(Entity, &Position)>,
    mut event_writer: EventWriter<LootPickupEvent>,
) {
    for (taker, player_position) in &mut entity_with_inventory {
        let mut player_scale = player_position.scale.clone();
        if player_scale.x.is_sign_negative() {
            player_scale.x *= -1.;
        }
        let player_hitbox = Aabb2d::new(
            player_position.translation.truncate(),
            player_scale.truncate() / 2.,
        );

        for (loot_entity, loot_position) in &loot {
            let loot_position = Vec3::new(loot_position.x, loot_position.y, 1.);
            let loot_hitbox = Aabb2d::new(loot_position.truncate(), Vec2::new(1., 1.) / 2.);

            if player_hitbox.intersects(&loot_hitbox) {
                event_writer.send(LootPickupEvent {
                    loot: loot_entity,
                    taker,
                });
            }
        }
    }
}

fn pickup_loot_handler(
    mut event_reader: EventReader<LootPickupEvent>,
    mut inventory: Query<&mut Inventory>,
    mut loot: Query<&mut Loot>,
    mut commands: Commands,
) {
    for event in event_reader.read() {
        let mut inventory = if let Ok(inventar) = inventory.get_mut(event.taker) {
            inventar
        } else {
            continue;
        };

        let mut loot = if let Ok(loot) = loot.get_mut(event.loot) {
            if loot.is_taken {
                continue;
            }

            loot
        } else {
            continue;
        };

        for item in &loot.loot {
            inventory.items.push(item.clone());
        }

        loot.is_taken = true;
        commands.entity(event.loot).despawn();
        log::warn!("despawn loot {}", event.loot);
    }
}

pub fn pickup_sound(
    mut event_reader: EventReader<LootPickupEvent>,
    asset_server: Res<AssetServer>,
    mut commands: Commands,
) {
    let mut play_sound = false;
    for _ in event_reader.read() {
        play_sound = true;
    }

    if play_sound {
        commands.spawn((
            AudioPlayer::new(asset_server.load("music/RPG Sound Pack/inventory/coin3.wav")),
            PlaybackSettings::DESPAWN,
        ));
    }
}

pub struct SimpleAnimation(Vec<(u32, usize, usize)>);

pub enum SpellAnimation {
    SIMPLE(SimpleAnimation),
    DIRECTIONAL {
        left: SimpleAnimation,
        right: SimpleAnimation,
        up: SimpleAnimation,
        down: SimpleAnimation,
    },
}

#[derive(Default, Resource)]
pub struct RespawnIntervals {
    next_respawn: HashMap<String, f32>,
}

impl RespawnIntervals {
    pub fn can_respawn(&self, name: &str, current: f32) -> bool {
        if let Some(spawn_frame) = self.next_respawn.get(name) {
            *spawn_frame <= current
        } else {
            true
        }
    }

    pub fn set_respawn(&mut self, name: &str, now: f32, duration: f32) {
        let overwrite = if let Some(old_respawn_frame) = self.next_respawn.get(name) {
            *old_respawn_frame <= now || *old_respawn_frame == 0.
        } else {
            true
        };

        if overwrite {
            self.next_respawn.insert(name.into(), now + duration);
        }
    }
}
