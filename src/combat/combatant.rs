use std::collections::BTreeSet;
use std::ops::{Add, Mul};

use bevy::asset::AssetServer;
use bevy::audio::{AudioPlayer, PlaybackSettings};
use bevy::math::Vec2;
use bevy::prelude::{
    Commands, Component, Entity, Event, EventReader, EventWriter, Mut, Query, Res, ResMut, Time,
    Virtual,
};

use crate::combat::status::StatusValue;
use crate::combat::RespawnIntervals;
use crate::hero::StatusEffect;

use super::{Loot, SpawnLootEvent};

#[derive(Component)]
pub struct Combatant {
    pub hp: StatusValue,
    pub mp: Option<StatusValue>,
    pub stamina: Option<StatusValue>,
    pub respawn_name: String,
    pub respawn_duration: f32,
    pub effects: Vec<StatusEffect>,
    pub last_status_update: f32,
}

impl Combatant {
    pub fn add_effect(&mut self, effect: StatusEffect) {
        self.effects.push(effect);
    }
}

#[derive(Event)]
pub struct CombatantDied {
    pub combatant: Entity,
    pub position: Vec2,
    pub loot: Loot,
    pub respawn_name: String,
    pub respawn_duration: f32,
}

pub fn spawn_loot_from_died_combatant(
    mut events: EventReader<CombatantDied>,
    mut event_writer: EventWriter<SpawnLootEvent>,
) {
    for e in events.read() {
        event_writer.send(SpawnLootEvent {
            position: e.position,
            loot: e.loot.to_owned(),
        });
    }
}

pub fn despawn_died_combatants(
    mut events: EventReader<CombatantDied>,
    mut commands: Commands,
    mut respawn_intervals: ResMut<RespawnIntervals>,
    time: Res<Time<Virtual>>,
) {
    let now = time.elapsed_secs();
    for event in events.read() {
        if let Some(mut combatant) = commands.get_entity(event.combatant) {
            combatant.despawn();
            log::warn!("despawn combatant {}", event.combatant);
            respawn_intervals.set_respawn(&event.respawn_name, now, event.respawn_duration);
        }
    }
}

pub fn combatant_died_sound(
    mut events: EventReader<CombatantDied>,
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    for _ in events.read() {
        let path = if rand::random() {
            "music/RPG Sound Pack/NPC/giant/giant2.wav"
        } else {
            "music/RPG Sound Pack/NPC/giant/giant4.wav"
        };

        commands.spawn((
            AudioPlayer::new(asset_server.load(path)),
            PlaybackSettings::DESPAWN,
        ));
    }
}

fn calc_changes_from_effects(combatant: &mut Mut<Combatant>, time_diff: f32) -> Vec<Changes> {
    let mut changes = vec![];
    let mut mp_limit = combatant.mp.as_ref().map(|mp| mp.missing()).unwrap_or(0.);

    for (index, effect) in combatant.effects.iter().enumerate() {
        match effect {
            StatusEffect::ManaRegeneration {
                amount,
                added_factor,
            } => {
                if mp_limit <= 0.01 {
                    continue;
                }

                let mut recovered = Changes::default();
                mp_recovery(
                    combatant.as_ref(),
                    &mut recovered,
                    time_diff,
                    Some(mp_limit),
                );
                let mut recovered = recovered * *added_factor;
                let recover_mp = recovered.mp;

                if let Some(recover_mp) = recover_mp {
                    let recover_mp = f32::min(recover_mp, *amount);
                    let recover_mp = f32::max(recover_mp, 0.);
                    recovered.used_effects.push((index, recover_mp));
                    changes.push(recovered);
                    mp_limit -= recover_mp;
                }
            }
        }
    }

    changes
}

fn mp_recovery(combatant: &Combatant, changes: &mut Changes, time_diff: f32, limit: Option<f32>) {
    if let Some(mp) = &combatant.mp {
        if let Some(stamina) = &combatant.stamina {
            let missing = mp.missing();
            let boost = 1. + mp.missing_fraction();

            let recover_mp = f32::min(boost * stamina.current / 2500. * time_diff, missing);
            let recover_mp = f32::min(recover_mp, stamina.current);
            let recover_mp = limit.map(|l| f32::min(recover_mp, l)).unwrap_or(recover_mp);

            changes.add_mp(recover_mp);
            changes.add_stamina(-recover_mp);
        }
    }
}
pub fn update_status(mut combatants: Query<&mut Combatant>, time: Res<Time<Virtual>>) {
    let now = time.elapsed_secs();

    for mut combatant in &mut combatants {
        let time_diff = now - combatant.last_status_update;

        let hp = &combatant.hp;
        let mut changes = Changes::default();
        if let Some(mp) = &combatant.mp {
            let missing = hp.missing();
            let boost = 1. + hp.missing_fraction();
            // 2% / sec => 1s / 50

            let recover_hp = f32::min(boost * mp.current * time_diff / 50., missing);

            changes.hp += recover_hp;
            changes.add_mp(-recover_hp);
        }
        mp_recovery(&combatant, &mut changes, time_diff, None);
        let _ = changes.apply_to(&mut combatant);

        let changes = calc_changes_from_effects(&mut combatant, time_diff);
        let mut remove_effects_indexes = BTreeSet::new();
        for change in changes {
            let id_s = change.apply_to(&mut combatant);
            if let Ok(mut id_s) = id_s {
                remove_effects_indexes.append(&mut id_s)
            }
        }
        for i in remove_effects_indexes.into_iter().rev() {
            combatant.effects.remove(i);
        }

        combatant.last_status_update = now;
    }
}

#[derive(Default)]
struct Changes {
    hp: f32,
    mp: Option<f32>,
    stamina: Option<f32>,
    used_effects: Vec<(usize, f32)>,
}

impl Changes {
    fn add_mp(&mut self, mp: f32) {
        self.mp = self.mp.map(|old| old + mp).or(Some(mp));
    }

    fn add_stamina(&mut self, stamina: f32) {
        self.stamina = self.stamina.map(|old| old + stamina).or(Some(stamina));
    }

    fn apply_to(mut self, combatant: &mut Combatant) -> Result<BTreeSet<usize>, ()> {
        if self.hp > 0. && self.hp > combatant.hp.missing() {
            return Err(());
        }
        if self.mp.is_some() != combatant.mp.is_some() {
            return Err(());
        }
        if self.stamina.is_some() != combatant.stamina.is_some() {
            return Err(());
        }
        if let Some(current) = &mut combatant.mp {
            if let Some(change) = self.mp {
                if change > 0. && change > current.missing() {
                    return Err(());
                }
            }
        }
        if let Some(current) = &mut combatant.stamina {
            if let Some(change) = self.stamina {
                if change > 0. && change > current.missing() {
                    return Err(());
                }
            }
        }

        combatant.hp.current += self.hp;
        if let Some(current) = &mut combatant.mp {
            if let Some(change) = self.mp {
                current.current += change;
            }
        }
        if let Some(current) = &mut combatant.stamina {
            if let Some(change) = self.stamina {
                current.current += change;
            }
        }

        let mut remove = BTreeSet::new();
        self.used_effects.sort_by(|l, r| l.0.cmp(&r.0));
        for (i, used) in self.used_effects.iter() {
            match &mut combatant.effects[*i] {
                StatusEffect::ManaRegeneration { amount, .. } => {
                    if *used >= *amount {
                        remove.insert(*i);
                    } else {
                        *amount -= *used;
                    }
                }
            }
        }

        Ok(remove)
    }
}

impl Add for Changes {
    type Output = Changes;

    fn add(self, rhs: Self) -> Self::Output {
        let mp = if let Some(l_mp) = self.mp {
            if let Some(r_mp) = rhs.mp {
                Some(l_mp + r_mp)
            } else {
                Some(l_mp)
            }
        } else {
            rhs.mp
        };

        let stamina = if let Some(l_stamina) = self.stamina {
            if let Some(r_stamina) = rhs.stamina {
                Some(l_stamina + r_stamina)
            } else {
                Some(l_stamina)
            }
        } else {
            rhs.stamina
        };

        let mut used_effects = self.used_effects.clone();
        used_effects.append(&mut rhs.used_effects.clone());

        Changes {
            hp: self.hp + rhs.hp,
            mp,
            stamina,
            used_effects,
        }
    }
}

impl Mul<f32> for Changes {
    type Output = Changes;

    fn mul(self, rhs: f32) -> Self::Output {
        let hp = self.hp * rhs;
        let mp = self.mp.map(|mp| mp * rhs);
        let stamina = self.stamina.map(|stamina| stamina * rhs);

        let used_effects = self
            .used_effects
            .iter()
            .map(|(i, val)| (*i, val * rhs))
            .collect();

        Self::Output {
            hp,
            mp,
            stamina,
            used_effects,
        }
    }
}
