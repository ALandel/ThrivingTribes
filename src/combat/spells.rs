use std::collections::HashMap;
use std::fs::{File, read_dir};
use std::io::Read;
use std::path::Path;

use bevy::asset::{Assets, AssetServer, Handle};
use bevy::prelude::{Image, Mesh, Res, ResMut, Resource};
use serde::Deserialize;

use crate::assets::create_cut_meshes;
use crate::combat::{SimpleAnimation, SpellAnimation};

#[derive(Resource, Default)]
pub struct GlobalSpellBook {
    pub spells: HashMap<String, Spell>,
}

pub struct Spell {
    pub animation: SpellAnimation,
    pub texture_handle: Handle<Image>,
    pub image_width: usize,
    pub image_height: usize,
    pub meshes_raw: Vec<Vec<Handle<Mesh>>>,
    pub speed: f32,
    pub cost: u16,
    pub cooldown_in_ms: u32,
    pub damage: DamageRatings,
}

pub struct DamageRatings {
    pub base: f32,
    pub variant_count: u8,
    pub variant: f32,
}


#[derive(Deserialize)]
#[serde(untagged)]
enum JsonAnimations {
    SIMPLE(Vec<[u32; 3]>),
    DIRECTIONAL {
        up: Vec<[u32; 3]>,
        down: Vec<[u32; 3]>,
        left: Vec<[u32; 3]>,
        right: Vec<[u32; 3]>,
    },
}

#[derive(Deserialize)]
struct JsonDamage {
    base: f32,
    variant_count: u8,
    variant: f32,
}

#[derive(Deserialize)]
struct JsonSpell {
    texture: String,
    image_width: usize,
    image_height: usize,
    speed: f32,
    animations: JsonAnimations,
    cost: u16,
    cooldown: f32,
    damage: JsonDamage,
}

pub fn load_spell<T: AsRef<Path>>(path: T, asset_server: &AssetServer, assets_meshes: &mut ResMut<Assets<Mesh>>) -> Spell {
    let mut json = String::new();
    File::open(path)
        .unwrap()
        .read_to_string(&mut json)
        .unwrap();

    let spell_data: JsonSpell = serde_json::from_str(&json).unwrap();

    let image_width = spell_data.image_width;
    let image_height = spell_data.image_height;
    let meshes_raw = create_cut_meshes(image_width, image_height, assets_meshes);
    let texture_handle = asset_server.load(&spell_data.texture);

    let animation = extract_animation(&spell_data);
    let cooldown_in_ms = spell_data.cooldown * 1000.;
    let cooldown_in_ms = cooldown_in_ms.round() as u32;

    Spell {
        texture_handle,
        image_width,
        image_height,
        meshes_raw,
        animation,
        speed: spell_data.speed,
        cooldown_in_ms,
        cost: spell_data.cost,
        damage: DamageRatings {
            base: spell_data.damage.base,
            variant_count: spell_data.damage.variant_count,
            variant: spell_data.damage.variant,
        },
    }
}

fn extract_animation(spell_data: &JsonSpell) -> SpellAnimation {
    match &spell_data.animations {
        JsonAnimations::SIMPLE(rows) => {
            let animations = rows
                .iter()
                .map(|frame| (frame[0], frame[1] as usize, frame[2] as usize))
                .collect();

            SpellAnimation::SIMPLE(SimpleAnimation(animations))
        }
        JsonAnimations::DIRECTIONAL { left, right, up, down } => {
            let left = left
                .iter()
                .map(|frame| (frame[0], frame[1] as usize, frame[2] as usize))
                .collect();
            let right = right
                .iter()
                .map(|frame| (frame[0], frame[1] as usize, frame[2] as usize))
                .collect();
            let up = up
                .iter()
                .map(|frame| (frame[0], frame[1] as usize, frame[2] as usize))
                .collect();
            let down = down
                .iter()
                .map(|frame| (frame[0], frame[1] as usize, frame[2] as usize))
                .collect();

            SpellAnimation::DIRECTIONAL {
                left: SimpleAnimation(left),
                right: SimpleAnimation(right),
                up: SimpleAnimation(up),
                down: SimpleAnimation(down),
            }
        }
    }
}

pub fn setup_global_spell_book(mut global_spell_book: ResMut<GlobalSpellBook>, asset_server: Res<AssetServer>, mut assets_meshes: ResMut<Assets<Mesh>>) {
    let files = read_dir("assets/spellbook/").unwrap();

    for file in files {
        let file = file.unwrap();
        let spell = load_spell(file.path(), &asset_server, &mut assets_meshes);
        let name = file.path().file_stem().unwrap().to_str().unwrap().to_string();

        global_spell_book.spells.insert(name, spell);
    }
}
