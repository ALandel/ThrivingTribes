use bevy::asset::{AssetServer, Assets};
use bevy::audio::{AudioPlayer, PlaybackSettings};
use bevy::math::bounding::{Aabb2d, IntersectsVolume};
use bevy::math::{Vec2, Vec3};
use bevy::prelude::{
    Color, ColorMaterial, Commands, Component, Entity, Event, EventReader, EventWriter, Gizmos,
    Mesh2d, Query, Res, ResMut, Time, Transform, Virtual, With, Without,
};
use bevy::sprite::MeshMaterial2d;

use crate::assets::Animation;
use crate::combat::combatant::Combatant;
use crate::combat::spells::GlobalSpellBook;
use crate::combat::{CombatantDied, SpellAnimation};
use crate::hero::PlayerCharacter;

use super::Loot;

#[derive(Component)]
pub struct Projectile {
    source: Entity,
    end_of_life: f32,
    power: f32,
    speed: f32,
    direction: crate::input::Direction,
    already_hit: Vec<Entity>,
    last_moved: f32,
}

impl Projectile {
    pub fn has_hit(&self, target: &Entity) -> bool {
        self.already_hit.contains(target)
    }

    pub fn add_hit(&mut self, target: Entity) {
        self.already_hit.push(target);
    }
}

#[derive(Event)]
pub struct FireProjectileEvent {
    pub source: Entity,
    pub transform: Transform,
    pub power: f32,
    pub spell: String,
    pub direction: crate::input::Direction,
}

#[derive(Event)]
pub struct ProjectileHitEvent {
    pub projectile: Entity,
    pub target: Entity,
    pub target_position: Vec2,
}

pub fn check_projectile_hits(
    mut combatants: Query<(Entity, &Transform), With<Combatant>>,
    projectile: Query<(Entity, &mut Projectile, &Transform), Without<Combatant>>,
    mut hit_event: EventWriter<ProjectileHitEvent>,
) {
    for (projectile_entity, projectile, projectile_trans) in &projectile {
        let projectil_hitbox = Aabb2d::new(
            projectile_trans.translation.truncate(),
            projectile_trans.scale.truncate() / 2.,
        );
        for (combatant_entity, combatant_trans) in &mut combatants {
            if combatant_entity == projectile.source {
                continue;
            }

            let mut combatant_scale = combatant_trans.scale.clone();
            if combatant_scale.x.is_sign_negative() {
                combatant_scale.x *= -1.;
            }

            let combatant_position = combatant_trans.translation.truncate();
            let combatant_hitbox = Aabb2d::new(combatant_position, combatant_scale.truncate() / 2.);

            if projectil_hitbox.intersects(&combatant_hitbox) {
                if !projectile.has_hit(&combatant_entity) {
                    hit_event.send(ProjectileHitEvent {
                        projectile: projectile_entity,
                        target: combatant_entity,
                        target_position: combatant_position,
                    });
                }
            }
        }
    }
}

pub fn draw_hitboxes(
    combatants: Query<&Transform, With<Combatant>>,
    projectile: Query<&Transform, With<Projectile>>,
    mut gizmos: Gizmos,
) {
    for projectile_trans in &projectile {
        gizmos.rect_2d(
            projectile_trans.translation.truncate(),
            projectile_trans.scale.truncate(),
            Color::hsl(0., 0.5, 0.5),
        );
    }

    for combatant_trans in &combatants {
        gizmos.rect_2d(
            combatant_trans.translation.truncate(),
            combatant_trans.scale.truncate(),
            Color::hsl(120., 0.5, 0.5),
        );
    }
}

pub fn projectile_hit_handler(
    mut events: EventReader<ProjectileHitEvent>,
    mut combatants: Query<(&mut Combatant, Option<&Loot>)>,
    mut projectiles: Query<&mut Projectile, Without<Combatant>>,
    mut combatant_died_event: EventWriter<CombatantDied>,
) {
    for event in events.read() {
        let (mut combatant, loot) = match combatants.get_mut(event.target) {
            Ok(combatant) => combatant,
            Err(err) => {
                eprintln!("{err}");
                continue;
            }
        };

        let mut projectile = match projectiles.get_mut(event.projectile) {
            Ok(projectile) => projectile,
            Err(err) => {
                eprintln!("{err}");
                continue;
            }
        };

        if combatant.hp.current <= 0. {
            continue;
        }

        let power = projectile.power;
        combatant.hp.current -= power;
        projectile.add_hit(event.target);

        if combatant.hp.current <= 0. {
            combatant_died_event.send(CombatantDied {
                combatant: event.target,
                position: event.target_position,
                loot: loot.map(|l| l.to_owned()).unwrap_or_else(|| Loot {
                    is_taken: true,
                    loot: vec![],
                }),
                respawn_name: combatant.respawn_name.clone(),
                respawn_duration: combatant.respawn_duration,
            });
        }
    }
}

pub fn projectile_hit_sound(
    mut events: EventReader<ProjectileHitEvent>,
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    for _ in events.read() {
        let path = if rand::random() {
            "music/RPG Sound Pack/misc/random2.wav"
        } else {
            "music/RPG Sound Pack/misc/random3.wav"
        };

        commands.spawn((
            AudioPlayer::new(asset_server.load(path)),
            PlaybackSettings::DESPAWN,
        ));
    }
}

pub fn despawn_projectile(
    projectile: Query<(Entity, &Projectile)>,
    mut commands: Commands,
    time: Res<Time<Virtual>>,
) {
    let now = time.elapsed_secs();
    for (entity, projectile) in &projectile {
        if projectile.end_of_life <= now {
            commands.entity(entity).despawn();
        }
    }
}

pub fn fire_projectile_sound(
    mut events: EventReader<FireProjectileEvent>,
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    let mut has_events = false;

    for _ in events.read() {
        has_events = true;
    }

    if has_events {
        commands.spawn((
            AudioPlayer::new(asset_server.load("music/RPG Sound Pack/battle/swing.wav")),
            PlaybackSettings::DESPAWN,
        ));
    }
}

pub fn update_projectile_position(
    mut position: Query<(&mut Transform, &mut Projectile), Without<PlayerCharacter>>,
    time: Res<Time<Virtual>>,
) {
    let now = time.elapsed_secs();
    for (mut transform, mut projectil) in &mut position {
        let time_diff = now - projectil.last_moved;
        let distance = projectil.speed * time_diff;
        projectil.last_moved = now;

        match projectil.direction {
            crate::input::Direction::RIGHT => transform.translation.x += distance,
            crate::input::Direction::LEFT => transform.translation.x -= distance,
            crate::input::Direction::UP => transform.translation.y += distance,
            crate::input::Direction::DOWN => transform.translation.y -= distance,
        }
    }
}

pub fn fire_projectile_handler(
    mut commands: Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    time: Res<Time<Virtual>>,
    mut events: EventReader<FireProjectileEvent>,
    global_spell_book: Res<GlobalSpellBook>,
) {
    let now = time.elapsed_secs();
    for event in events.read() {
        let spell = &global_spell_book.spells[&event.spell];
        let mut animation = Animation::new(now, event.direction);

        let mut duration_in_ms = 0;
        match &spell.animation {
            SpellAnimation::SIMPLE(spell_animation) => {
                for a in &spell_animation.0 {
                    duration_in_ms += a.0;
                    animation.add(None, a.0, spell.meshes_raw[a.1][a.2].clone());
                }
            }
            SpellAnimation::DIRECTIONAL {
                up,
                down,
                left,
                right,
            } => {
                use crate::input::Direction::*;
                for (direction, spell_animation) in
                    [(UP, up), (DOWN, down), (LEFT, left), (RIGHT, right)]
                {
                    for a in &spell_animation.0 {
                        duration_in_ms += a.0;
                        animation.add(
                            Some(direction.clone()),
                            a.0,
                            spell.meshes_raw[a.1][a.2].clone(),
                        );
                    }
                }
            }
        }

        let transform = event.transform.clone().with_scale(Vec3::splat(32.));

        commands.spawn((
            Projectile {
                power: event.power,
                source: event.source,
                end_of_life: now + (duration_in_ms as f32 / 1000.),
                speed: spell.speed,
                direction: event.direction,
                already_hit: vec![],
                last_moved: now,
            },
            animation,
            Mesh2d(spell.meshes_raw[0][0].clone().into()),
            MeshMaterial2d(materials.add(ColorMaterial::from(spell.texture_handle.clone()))),
            transform,
        ));
    }
}
