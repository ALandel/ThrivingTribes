use crate::combat::combatant::Combatant;
use crate::hero::PlayerCharacter;
use crate::model::UI;
use bevy::asset::Assets;
use bevy::ecs::system::EntityCommands;
use bevy::prelude::{
    Color, ColorMaterial, Commands, Component, Entity, Mesh, Mesh2d, Query, ResMut, Text,
    Transform, Visibility, With, Without,
};
use bevy::render::mesh::RectangleMeshBuilder;
use bevy::sprite::MeshMaterial2d;

pub struct StatusValue {
    pub current: f32,
    pub max: f32,
}

impl StatusValue {
    pub fn new(current: f32, max: f32) -> Self {
        Self { current, max }
    }

    pub fn fraction(&self) -> f32 {
        self.current / self.max
    }

    pub fn missing(&self) -> f32 {
        f32::max(self.max - self.current, 0.)
    }

    pub fn missing_fraction(&self) -> f32 {
        self.missing() / self.max
    }
}

pub const STATUS_HEIGHT_PLAYER: f32 = 8.;
pub const STATUS_HEIGHT_NPC: f32 = STATUS_HEIGHT_PLAYER / 2.;
pub const STATUS_WIDTH_PLAYER: (f32, f32) = (100., 49.);
pub const STATUS_WIDTH_NPC: (f32, f32) = (50., 24.);

pub enum StatusBarTyp {
    HP,
    MP,
    STAMINA,
}

#[derive(Component)]
pub struct StatusBar {
    pub combatant: Entity,
    pub typ: StatusBarTyp,
    pub size: f32,
}

pub fn draw_and_update_status_bars(
    mut commands: Commands,
    mut status_bar: Query<(Entity, &mut Transform, &StatusBar)>,
    combatant: Query<(&Combatant, &mut Transform), Without<StatusBar>>,
) {
    for (entity, mut bar_transform, status_bar) in &mut status_bar {
        let combatant_id = status_bar.combatant;
        match combatant.get(combatant_id) {
            Ok((combatant, combatant_transform)) => {
                match status_bar.typ {
                    StatusBarTyp::HP => {
                        let fraction = combatant.hp.fraction();
                        bar_transform.scale.x = fraction;
                        bar_transform.translation.x = combatant_transform.translation.x;
                        bar_transform.translation.y = combatant_transform.translation.y + 42.;
                    }
                    StatusBarTyp::MP => {
                        if let Some(mp) = &combatant.mp {
                            let fraction = mp.fraction();
                            bar_transform.scale.x = fraction;
                            bar_transform.translation.x =
                                combatant_transform.translation.x - status_bar.size / 2.;
                            bar_transform.translation.y = combatant_transform.translation.y + 32.;
                        }
                    }
                    StatusBarTyp::STAMINA => {
                        if let Some(stamina) = &combatant.stamina {
                            let fraction = stamina.fraction();
                            bar_transform.scale.x = fraction;
                            bar_transform.translation.x =
                                combatant_transform.translation.x + status_bar.size / 2.;
                            bar_transform.translation.y = combatant_transform.translation.y + 32.;
                        }
                    }
                };
            }
            Err(..) => {
                commands.entity(entity).despawn();
            }
        }
    }
}

#[derive(Component)]
pub struct StatusEffectText;

pub fn print_status_effects(
    player: Query<&Combatant, With<PlayerCharacter>>,
    old_texts: Query<Entity, With<StatusEffectText>>,
    // asset_server: Res<AssetServer>,
    mut commands: Commands,
) {
    for old in &old_texts {
        commands.entity(old).despawn();
    }

    let player = match player.get_single() {
        Ok(player) => player,
        Err(err) => {
            eprintln!("in print_items: {err}");
            return;
        }
    };

    let mut new_texts_raw = Vec::with_capacity(player.effects.len());
    for item in &player.effects {
        let name = format!("{item:?}");
        new_texts_raw.push(name);
    }
    new_texts_raw.sort();

    let text = new_texts_raw.join("\n");
    let text = Text::new(text);

    commands.spawn((UI, StatusEffectText, text));
}

fn color(status_bar_typ: &StatusBarTyp) -> Color {
    match status_bar_typ {
        StatusBarTyp::HP => Color::hsla(0., 1., 0.5, 0.9),
        StatusBarTyp::MP => Color::hsla(240., 1., 0.5, 0.9),
        StatusBarTyp::STAMINA => Color::hsla(60., 1., 0.5, 0.8),
    }
}

// TODO: move calculations of meshes and position into spawn_status_bar
pub fn spawn_status_bar<'a>(
    commands: &'a mut Commands,
    typ: StatusBarTyp,
    meshes: &mut ResMut<Assets<Mesh>>,
    materials: &mut ResMut<Assets<ColorMaterial>>,
    combatant: Entity,
    mesh: RectangleMeshBuilder,
    position: Transform,
    size: f32,
) -> EntityCommands<'a> {
    let color = color(&typ);
    let status_bar = commands.spawn((
        StatusBar {
            combatant,
            typ,
            size,
        },
        Mesh2d(meshes.add(mesh).into()),
        MeshMaterial2d(materials.add(color)),
        position,
        Visibility::Inherited,
    ));

    status_bar
}
