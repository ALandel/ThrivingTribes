use std::time::Duration;

use bevy::app::{App, Startup, Update};
use bevy::prelude::*;

use crate::combat::combatant::Combatant;
use crate::hero::PlayerCharacter;
use crate::systems::{debug_output, setup_debug_output};

pub struct DebugPlugin;

impl Plugin for DebugPlugin {
    fn build(&self, app: &mut App) {
        app
            .add_systems(Startup, setup_debug_output)
            .add_systems(Update, debug_output)
            .init_resource::<HeroDebugInfo>()
            .add_systems(Update, Self::debug_hero_status.before(debug_output),
            );
    }
}

#[derive(Component, Default)]
pub struct DebugInfo {
    pub text: Vec<(String, String)>
}

#[derive(Resource, Default)]
struct HeroDebugInfo {
    old_hp: f32,
    hp_diff: f32,
    old_mp: f32,
    mp_diff: f32,
    old_stamina: f32,
    stamina_diff: f32,
    time: Duration,
}

impl DebugPlugin {
    fn debug_hero_status(
        hero: Query<&Combatant, With<PlayerCharacter>>,
        mut info: ResMut<HeroDebugInfo>,
        time: Res<Time<Virtual>>,
        mut debug: Query<&mut DebugInfo>,
    ) {
        let now = time.elapsed();
        let time_diff = now - info.time;
        // let mut info = info.as_deref_mut();

        for combatant in &hero {
            for mut debug in &mut debug {
                if time_diff >= Duration::from_millis(250) {
                    info.hp_diff = (combatant.hp.current - info.old_hp) * 1000. / time_diff.as_millis() as f32;
                    info.old_hp = combatant.hp.current;

                    if let Some(mp) = &combatant.mp {
                        info.mp_diff = (mp.current - info.old_mp) * 1000. / time_diff.as_millis() as f32;
                        info.old_mp = mp.current;
                    }

                    if let Some(stamina) = &combatant.stamina {
                        info.stamina_diff = (stamina.current - info.old_stamina) * 1000. / time_diff.as_millis() as f32;
                        info.old_stamina = stamina.current;
                    }
                    info.time = now;
                }

                let hp_text = format!("| {:04.1}/{:4} {:+02.1} ", combatant.hp.current, combatant.hp.max, info.hp_diff);
                debug.text.push(("HP".to_string(), hp_text));

                if let Some(mp) = &combatant.mp {
                    let mp_text = format!("| {:04.1}/{:4} {:+02.1} ", mp.current, mp.max, info.mp_diff);
                    debug.text.push(("MP".to_string(), mp_text));
                }

                if let Some(stamina) = &combatant.stamina {
                    let text = format!("| {:06.1}/{:6} {:+02.1} ", stamina.current, stamina.max, info.stamina_diff);
                    debug.text.push(("Stamina".to_string(), text));
                }
            }
        }
    }
}